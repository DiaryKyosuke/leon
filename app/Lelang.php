<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lelang extends Model
{
	protected $table = 'lelang';
	protected $primaryKey = 'id_lelang';
	protected $fillable = ['id_barang','tgl_selesai_lelang','harga_akhir','id_user','id_petugas','status'];
}
