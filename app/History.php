<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table = 'history_lelang';
	protected $primaryKey = 'id_history';
	protected $fillable = ['id_lelang','id_barang','id_user','tawaran'];
}
