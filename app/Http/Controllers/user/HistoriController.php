<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
class HistoriController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::guard('user')->user()->id;
        $barang = DB::table('ikut')
        ->join('barang','barang.id_barang','=','ikut.id_barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->where('ikut.id_user','=',$id)
        ->get();
         
         $users = DB::table('users')->where('id','=',$id)->first();

         $nama = DB::table('users')->get();
        return view('user/history',compact('barang','users','nama'))->with('i');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function rincian($id_barang, $id_user)
    {
        $barang = DB::table('barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->where('barang.id_barang','=',$id_barang)
        ->first();
        
        $users = DB::table('users')
        ->where('id','=',$id_user)
        ->first();

        $tawar = DB::table('users')
        ->join('history_lelang','history_lelang.id_user','=','users.id')
        ->where('history_lelang.id_barang','=',$id_barang)
        ->orderBy('history_lelang.created_at','DESC')
        ->get();

        return view('user/history_rincian',compact('barang','users','tawar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
