<?php

namespace App\Http\Controllers\user;

use DB;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::guard('user')->user()->id;
        $data = User::where('id','=', $id)->first();
        return view('user/profil',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function edit_password(Request $request)
    {
        $data = Auth::guard('user')->attempt(['id' => $request->id, 'password' => $request->pw_lama]);
        if ($data == false) {
            alert()->error('Password Lama Salah!!','Error')->persistent("OK");
            return redirect()->back();
        }
        else if ($data == true) {
            $pw = Hash::make($request->pw_baru);
            $r = User::find($request->id);
            $r->password = $pw;
            $r->save();
            alert()->success('Sandi Berhasil Diganti!!','Sukses')->persistent("OK");
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_user(request $request)
    {
        $data = User::find($request->id);
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->telp = $request->telp;
        $data->save();
        alert()->success('Data Baru Berhasil Disimpan!!','Tersimpan')->persistent("OK");
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
