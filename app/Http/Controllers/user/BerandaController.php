<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\History;
use App\Barang;
use App\Lelang;
use App\Ikut;
use Auth;
use DB;
class BerandaController extends Controller
{
    public function akan()
    {
        $barang = DB::table('barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->orWhere('lelang.status','=','Akan Datang')
        ->orderBy('barang.tgl_mulai_lelang','ASC')
        ->paginate(8);
        
        $nol = 0;
        $nols = 0;
        $angka = 1;
        return view('akan',compact('barang','nol','nols','angka'))->with('i');
    }

    public function detail($id_barang)
    {
        $barang = DB::table('barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->where('barang.id_barang','=',$id_barang)
        ->first();
        return view('detail_barang',compact('barang'));
    }

    public function index()
    {
        $barang = DB::table('barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->where('lelang.status','=','Dibuka')
        ->get();
        $angka = 1;
        $nol = 0;
        $nols = 0;
        return view('user/beranda',compact('barang','angka','nol','nols'))->with('i');
    }

    public function coming()
    {
        $barang = DB::table('barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->where('lelang.status','=','Akan Datang')
        ->orderBy('barang.tgl_mulai_lelang','ASC')
        ->paginate(8);
        $angka = 1;
        $nol = 0;
        $nols = 0;
        return view('user/coming',compact('barang','angka','nol','nols'))->with('i');
    }

    public function running()
    {
        $barang = DB::table('barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->where('lelang.status','=','Dibuka')
        ->orderBy('barang.tgl_mulai_lelang','ASC')
        ->paginate(8);
        $angka = 1;
        $nol = 0;
        $nols = 0;
        return view('user/running',compact('barang','angka','nol','nols'))->with('i');
    }

    public function statusup(Request $request)
    {
        
      if (count($request->id_lelang) > 0 ){
        foreach ($request->id_lelang as $item => $v) {
            $status = array(
                'status' => 'Dibuka'
            );
            DB::table('lelang')->where('id_lelang','=', $request->id_lelang[$item])->update($status);
        }
    }
    $ba= "sukses";
    return response()->json($ba);
}

public function ikuti(Request $request)
{
    $ikuti = new Ikut();
    $ikuti->id_user = $request->id_user;
    $ikuti->id_barang = $request->id_barang;
    $ikuti->status = "Diikuti";
    $ikuti->save();
    return response ()->json("sukses");
}

public function info($id_barang, $id)
{
    $barang = DB::table('barang')
    ->join('lelang','lelang.id_barang','=','barang.id_barang')
    ->where('barang.id_barang','=',$id_barang)
    ->first();

    $users = DB::table('users')
    ->where('id','=',$id)
    ->first();

    $ikut = DB::table('ikut')
    ->where([['id_user', '=', $id],['id_barang', '=',$id_barang]])
    ->first();

    if ($ikut == null) {
        $status = "null";
    }
    else if ($ikut->id_barang == $id_barang) {
        $status = "Diikuti";
    }
    else{
        $status = "null";
    }

    $tawar = DB::table('users')
    ->join('history_lelang','history_lelang.id_user','=','users.id')
    ->where('history_lelang.id_barang','=',$id_barang)
    ->orderBy('history_lelang.created_at','DESC')
    ->get();

    return view('user/tawar',compact('barang','users','status','tawar'));
}


public function tawar_barang(Request $request)
{
    $barang = DB::table('barang')
    ->where('id_barang','=',$request->id_barang)
    ->first();

    $max = DB::table('history_lelang')
    ->where('id_barang','=',$request->id_barang)
    ->get()
    ->max('penawaran_harga');

    if ($max == null) {
        $asli = $barang->harga_awal;
    }else{
        $asli = $max;
    }

    $t = $request->tawars;
    $tawars = preg_replace("/[^0-9]/", "", $t);

    if ($tawars < $asli || $tawars == $asli) {
        $data = ["error", $asli];
        return response()->json($data);
    }
    else{
        $tawar = new History();
        $tawar->id_lelang = $request->id_lelang;
        $tawar->id_barang = $request->id_barang;
        $tawar->id_user = $request->id_user;
        $tawar->penawaran_harga = $tawars;
        $tawar->save();
        return response ()->json();
    }
}

public function ditutup(Request $request)
{
    $max = DB::table('history_lelang')
    ->where('id_barang','=',$request->id_barang)
    ->get()
    ->max('penawaran_harga');

    $menang = DB::table('history_lelang')
    ->where([['id_barang', '=',$request->id_barang],['penawaran_harga', '=', $max]])
    ->first();

    $lelang = Lelang::find($request->id_lelang);
    if ($max == null || $menang == null) {

    }else{
        $lelang->harga_akhir = $max;
        $lelang->id_user = $menang->id_user;
    }
    $lelang->status = "Ditutup";
    $lelang->save();
    $ba ="sukses";
    return response()->json($ba);
}
}
