<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Barang;
use Validator;
Use App\Lelang;
use Carbon\Carbon;
class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $barang = DB::table('barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->get();
        return view('admin/barang',compact('barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'barang' => 'required',
          'fotos' => 'required',
          'harga' => 'required',
      ]);
        $file = $request->file('fotos');
        $nama_file = $file->getClientOriginalName();
        $file->move('foto/barang',$nama_file);
        $rego = $request->harga;
        $harga = preg_replace("/[^0-9]/", "", $rego);
        $deskripsi = nl2br($request->deskripsi);
        $des = str_replace('<br />', '<br>', $deskripsi);

        $barang = Barang::create([
            'nama_barang' => $request->barang,
            'foto_barang' => $nama_file,
            'harga_awal' => $harga,
            'deskripsi_barang' => $des
        ])->id_barang;
        
        Lelang::create([
            'id_barang' => $barang,
            'status' => "Belum Diatur"
        ]);
        
        $ba = DB::table('barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->where('barang.id_barang' ,'=', $barang)
        ->first();
        return response ()->json($ba);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'barang' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required'
        ]);
        $rego = $request->harga;
        $harga = preg_replace("/[^0-9]/", "", $rego);
        $barang = Barang::find($request->id_barang);
        $barang->nama_barang = $request->barang;
        $deskripsi = nl2br($request->deskripsi);
        $des = str_replace('<br />', '<br>', $deskripsi);
        if ($request->fotos == null) {

        } else {
            $file = $request->file('fotos');
            $nama_file = $file->getClientOriginalName();
            $file->move('foto/barang',$nama_file);
            $barang->foto_barang = $nama_file;
        }
        $barang->harga_awal = $harga;
        $barang->deskripsi_barang = $des;
        $barang->save();

        $ba = DB::table('barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->where('barang.id_barang' ,'=', $request->id_barang)
        ->first();
        return response ()->json($ba);
    }


    public function nglelang(Request $request)
    {
        $this->validate($request, [
            'tgl_awal' => 'required',
            'jam_awal' => 'required',
            'tgl_akhir' => 'required',
            'jam_akhir' => 'required'
        ]);
        $awal = Carbon::parse($request->tgl_awal)->formatLocalized('%Y%m%d');
        $uwes = Carbon::parse($request->tgl_akhir)->formatLocalized('%Y%m%d');
        $tgl_mulai = Carbon::parse("$request->tgl_awal $request->jam_awal")->formatLocalized('%Y-%m-%d %H:%M');
        $tgl_selesai = Carbon::parse("$request->tgl_akhir $request->jam_akhir")->formatLocalized('%Y-%m-%d %H:%M');
        $now = Carbon::now();
        $lekas = Carbon::parse("$request->tgl_awal $request->jam_awal")->formatLocalized('%Y%m%d%H%M');
        $sekarang = Carbon::parse("$now")->formatLocalized('%Y%m%d%H%M');

        if ($awal > $uwes){
            $ba = 0;
            return response ()->json($ba);
        }else{
            if ($sekarang > $lekas) {
                $status = "Dibuka";
            } else if($sekarang < $lekas) {
                $status = "Akan Datang";
            }
            $barang = Barang::find($request->id_barang);
            $barang->tgl_mulai_lelang = $tgl_mulai;
            $barang->save();

            $lelang = Lelang::find($request->id_lelang);
            $lelang->tgl_selesai_lelang = $tgl_selesai;
            $lelang->id_petugas = $request->id_petugas;
            $lelang->status = $status;
            $lelang->save();

            $ba = DB::table('barang')
            ->join('lelang','lelang.id_barang','=','barang.id_barang')
            ->where('barang.id_barang' ,'=', $request->id_barang)
            ->first();
            return response ()->json($ba);
        }
        
    }

    public function stop_lelang(Request $request)
    {
        $max = DB::table('history_lelang')
        ->where('id_barang','=',$request->id_barang)
        ->get()
        ->max('penawaran_harga');

        $menang = DB::table('history_lelang')
        ->where([['id_barang', '=',$request->id_barang],['penawaran_harga', '=', $max]])
        ->first();

        $lelang = Lelang::find($request->id_lelang);
        if ($max == null || $menang == null) {
               
        }else{
            $lelang->harga_akhir = $max;
            $lelang->id_user = $menang->id_user;
        }
        $lelang->status = "Ditutup";
        $lelang->save();
        $ba = DB::table('barang')
            ->join('lelang','lelang.id_barang','=','barang.id_barang')
            ->where('barang.id_barang' ,'=', $request->id_barang)
            ->first();
        return response()->json($ba);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Barang::find($request->id_barang)->delete();
        return response()->json();
    }
}
