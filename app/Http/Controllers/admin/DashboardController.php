<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use App\User;
use App\Barang;
use App\Lelang;
use App\Admin;
use Validator;
class DashboardController extends Controller
{
    
    public function index()
    {
        $barang = Barang::count();
        $datang = Lelang::where('status','=' ,'Akan Datang')->count();
        $buka = Lelang::where('status','=' ,'Dibuka')->count();
        $tutup = Lelang::where('status','=' ,'Ditutup')->count();

        $barangs = DB::table('barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->where('lelang.status', '=', 'Ditutup')
        ->get();
        $users = DB::table('users')->get();

        return view('admin/dashboard',compact('barang','datang','buka','tutup','barangs','users'))->with('i');
    }


    public function petugas()
    {
        $admin = Admin::get();
        if (Auth::guard('admin')->user()->id_level == 1) {
            return view('admin/petugas',compact('admin'))->with('i');
        } else {
            return redirect()->back();
        }
        
    }


    public function store_petugas(Request $request)
    {
        $rules = array (
            'nama' => 'required',
            'email' => 'required',
            'username' => 'required',
            'password' => 'required',
            'id_level' => 'required'
        );
        $pw = Hash::make($request->password);

        $validator = Validator::make( Input::all (), $rules );
        if ($validator->fails())
            return Response::json ( array (
                'errors' => $validator->getMessageBag ()->toArray()
            ) );
        else {
            $admin = new Admin ();
            $admin->nama = $request->nama;
            $admin->email = $request->email;
            $admin->username = $request->username;
            $admin->password = $pw;
            $admin->id_level = $request->id_level;
            $admin->save();
            return response ()->json( $admin );
        }
    }


    public function edit_petugas(Request $request)
    {
        $admin = Admin::find($request->id_petugas);
        $admin->nama = $request->nama;
        $admin->email = $request->email;
        $admin->username = $request->username;
        $admin->id_level = $request->id_level;
        $admin->save();
        return response ()->json($admin);
    }


    public function delete_petugas(Request $request)
    {
        Admin::find ($request->id_petugas)->delete();
        return response()->json();
    }

}
