<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Auth;
use App\Admin;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::guard('admin')->user()->id_petugas;
        $data = Admin::where('id_petugas','=', $id)->first();
        return view('admin/profile',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function ubah_akun(Request $request)
    {
        $data = Admin::find($request->id_petugas);
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->save();
        alert()->success('Data Baru Berhasil Disimpan!!','Tersimpan')->persistent("OK");
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ubah_pw(Request $request)
    {
        $data = Auth::guard('admin')->attempt(['id_petugas' => $request->id_petugas, 'password' => $request->pw_lama]);
        if ($data == false) {
            alert()->error('Password Lama Salah!!','Error')->persistent("OK");
            return redirect()->back();
        }
        else if ($data == true) {
            $pw = Hash::make($request->pw_baru);
            $r = Admin::find($request->id_petugas);
            $r->password = $pw;
            $r->save();
            alert()->success('Sandi Berhasil Diganti!!','Sukses')->persistent("OK");
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
