<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use PDF;
use DB;
class LaporanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/cetak_laporan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cetak_pdf(Request $request)
    {
        $awal = Carbon::parse($request->awal)->formatLocalized('%Y-%m-%d');
        $akhir = Carbon::parse($request->akhir)->formatLocalized('%Y-%m-%d');

        $history = DB::table('barang')
        ->join('lelang','lelang.id_barang','=','barang.id_barang')
        ->where('tgl_selesai_lelang','>=',$awal)
        ->Where('tgl_selesai_lelang','<=', $akhir)
        ->where('lelang.status', '=', 'Ditutup')
        ->get()
        ->sortBy('tgl_selesai_lelang');
        $users = DB::table('users')->get();

        $pdf = PDF::loadview('admin/view_pdf',compact('history','users','awal','akhir'))->setPaper('a4', 'landscape');
        return $pdf->stream();
        // return $pdf->download('Laporan Peleangan Barang.pdf');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
