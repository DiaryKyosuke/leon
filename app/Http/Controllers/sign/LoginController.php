<?php

namespace App\Http\Controllers\sign;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use DB;
class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sign/login');
    }

    public function logadmin()
    {
        return view('sign/logadmin');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function melbu(Request $request)
    {
        $this->validate($request, [
          'username' => 'required',
          'password' => 'required'
        ]);
        
        $admin = Auth::guard('admin')->attempt(['username' => $request->username, 'password' => $request->password]);
        if ($admin == false) {
           alert()->error('Username Dan Sandi Salah!!','Error')->persistent("OK");
           return redirect('logadmin');
        }else if ($admin == true) {
           alert()->success('Login Sukses!!','Berhasil')->persistent("OK");
           return redirect()->intended('dashboard');
        }
    }


   public function masuk(Request $request)
   {

        $this->validate($request, [
          'username' => 'required',
          'password' => 'required'
        ]);
        $users = Auth::guard('user')->attempt(['username' => $request->username, 'password' => $request->password]);
        if($users == false){
           alert()->error('Username Dan Sandi Salah!!','Error')->persistent("OK");
           return redirect('sign');
        }else if ($users == true) {
        alert()->success('Login Sukses!!','Berhasil')->persistent("OK");
        return redirect()->intended('beranda');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function logout()
    {
        if (Auth::guard('admin')->check()) {
          Auth::guard('admin')->logout();
      }
      elseif (Auth::guard('user')->check()) {
          Auth::guard('user')->logout();
      }

      return redirect('/');

  }
}
