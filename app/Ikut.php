<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ikut extends Model
{
    protected $table = 'ikut';
	protected $primaryKey = 'id_ikut';
	protected $fillable = ['id_user','id_barang','status'];
}
