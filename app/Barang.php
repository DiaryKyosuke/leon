<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barang';
	protected $primaryKey = 'id_barang';
	protected $fillable = ['nama_barang','foto_barang','tgl_mulai_lelang','harga_awal','deskripsi_barang'];
}
