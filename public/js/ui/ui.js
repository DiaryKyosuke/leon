
window.addEventListener('load', function () {
	$("#preloader").fadeOut("slow");
});
window.onscroll = function() {myFunction()};
var navbar = document.getElementById("navbar");
var sticky = navbar.offsetTop;
function myFunction() {
	if (window.pageYOffset >= sticky) {
		navbar.classList.add("sticky")
	} else {
		navbar.classList.remove("sticky");
	}
}
var wow = new WOW ({
	offset:       75,         
	mobile:       false,       
});
wow.init();