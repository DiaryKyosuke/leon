// start       
$(document).ready(function () {
  $.ajaxSetup({
   headers: {
     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
   }
 });
// create data

// set value id level
$('#Admin').on('click',function() {
  $('#level').val("1");
});
$('#Petugas').on('click',function() {
  $('#level').val('2');
});
// end

$('.lihat').click(function(){
  if($(this).is(':checked')){
    $('.pw').attr('type','text');
  }else{
    $('.pw').attr('type','password');
  }
});
$("#m").on('click',function(e) {

  e.preventDefault();

  var password = $("#pas").val();
  var confirmPassword = $("#kon").val();

  if (password != confirmPassword) {
    swal({
      icon: 'error',
      title: 'Oops...',
      text: 'Sandi Yang Anda Masukkan Tidak Cocok!'
    });
    return false;
  } 
  $.ajax({
    url:'/store_petugas',
    type:"POST",
    data: {
      'nama'     : $('input[name="nama"]').val(),
      'email'    : $('input[name="email"]').val(),
      'username' : $('input[name="username"]').val(),
      'password' : $('input[name="password"]').val(),
      'id_level' : $('input[name="id_level"]').val(),
    },
    dataType: "json",
    error:function(){
      swal({
        icon: 'error',
        title: 'Error',
        text: 'Data Harus Diisi'
      });
    },
    success: function(data) {
      $(".dataTables_empty").remove();
      var no =document.getElementById("example").rows.length;
      document.getElementById("sample_form").reset();
      $('.pw').attr('type','password');
      
      swal({
        icon: 'success',
        title: 'Sukses',
        text: 'Data Berhasil Ditambahkan'
      });
      if (data.id_level == 1) {
        $level = "Administrator";
        $w = "primary";
      } else {
        $level = "Petugas";
        $w = "success";
      }
      $('#example').append(
        "<tr class='item"+ data.id_petugas +"''>"+
        // "<td width='2px;'>"+ no +"</td>"+
        "<td width='150px;'>"+
        "<span class='btn-sm btn-"+ $w +" lev'>"+ $level +"</span>"+
        "</td>"+
        "<td>"+ data.nama +"</td>"+
        "<td>"+ data.email +"</td>"+
        "<td>"+ data.username +"</td>"+
        "<td>"+
        "<button class='btn btn-outline-primary btn-sm btn-ling edit-modal' data-id_petugas='"+data.id_petugas+"' " +
        "data-id_level='"+data.id_level+"' data-name='"+data.nama+"' data-email='"+data.email+"' data-username='"+data.username+"'>"+
        "<i class='fas fa-user-edit'></i>"+
        "</button>"+
        " <button class='btn btn-outline-danger btn-sm btn-ling delete-modal' data-id='"+data.id_petugas+"' data-name='"+data.nama+"'>"+
        "<i class='fas fa-trash-alt'></i>"+
        "</button>"+
        "</td>"+
        "</tr>");
    },
  });
});
// end create data

// edit data... jangan pakai tag form dihmtl
$(document).on('click', '.edit-modal', function() {
  $('#admin').on('click',function() {
    $('.id_lev').val("1");
  });
  $('#petugas').on('click',function() {
    $('.id_lev').val('2');
  });

  $('.id_petugas').attr("value",$(this).data('id_petugas'));
  $('.nama').attr("value",$(this).data('name'));
  $('.email').attr("value",$(this).data('email'));
  $('.username').attr("value",$(this).data('username'));
  $('#edit').modal('show');

  var level = $(this).data('id_level');
  if (level == 1) {
    $('#admin').prop("checked", "checked");
    $('.id_lev').val('1');
  } else {
    $('#petugas').prop("checked", "checked");
    $('.id_lev').val('2');
  }
});

$('.modal-footer').on('click','.edit',function(e) {
  e.preventDefault();
  $.ajax({
    type: 'post',
    url: '/edit_petugas',
    data: {
      '_token'    : $('input[name=_token]').val(),
      'id_petugas': $('.id_petugas').val(),
      'nama'      : $('.nama').val(),
      'email'     : $('.email').val(),
      'username'  : $('.username').val(),
      'id_level'  : $('.id_lev').val()
    },
    dataType: "json",
    success: function(data) {
      swal({
        icon: 'success',
        title: 'Sukses',
        text: 'Data Berhasil Diubah'
      });
      if (data.id_level == 1) {
        $level = "Administrator";
        $w = "primary";
      } else {
        $level = "Petugas";
        $w = "success";
      }

      $('.item' + data.id_petugas).replaceWith(
        "<tr class='item"+ data.id_petugas +"''>"+
        // "<td width='2px;'>"+ no +"</td>"+
        "<td width='150px;'>"+
        "<span class='btn-sm btn-"+ $w +" lev'>"+ $level +"</span>"+
        "</td>"+
        "<td>"+ data.nama +"</td>"+
        "<td>"+ data.email +"</td>"+
        "<td>"+ data.username +"</td>"+
        "<td>"+
        "<button class='btn btn-outline-primary btn-sm btn-ling edit-modal' data-id_petugas='"+data.id_petugas+"' " +
        "data-id_level='"+data.id_level+"' data-name='"+data.nama+"' data-email='"+data.email+"' data-username='"+data.username+"'>"+
        "<i class='fas fa-user-edit'></i>"+
        "</button>"+
        " <button class='btn btn-outline-danger btn-sm btn-ling delete-modal' data-id='"+data.id_petugas+"' data-name='"+data.nama+"'>"+
        "<i class='fas fa-trash-alt'></i>"+
        "</button>"+
        "</td>"+
        "</tr>");

    }
  });
});

// end edit data


// delete data
$(document).on('click', '.delete-modal', function() {
  $('.did').text($(this).data('id'));
  $('.dname').html($(this).data('name'));
  $('#myModal').modal('show');
});
$('.modal-footer').on('click', '.delete',function(e) {
  e.preventDefault();
  $.ajax({
    type: 'post',
    url: '/delete_petugas',
    data: {
      '_token': $('input[name=_token]').val(),
      'id_petugas': $('.did').text()
    },
    success: function(data) {
      swal({
        icon: 'success',
        title: 'Sukses',
        text: 'Data Berhasil Dihapus'
      });
      $('.item' + $('.did').text()).remove();
    }
  });
});
// end delete data
});