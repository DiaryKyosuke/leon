// fungsi untuk menampilkan gambar sebelum diupload di bagian tambah data
function fileValidation(){
  var fileInput = document.getElementById('file');
  var filePath = fileInput.value;
  var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
  if(!allowedExtensions.exec(filePath)){
    swal({
      icon: 'error',
      title: 'Ekstensi Salah',
      text: 'Gunakan Ekstensi .jpeg/.jpg/.png/.gif only.'
    });
    fileInput.value = '';
    return false;
  }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            document.getElementById('imagePreview').innerHTML = '<a href="'+e.target.result+'" data-fancybox><img id="gimbir" style="max-height: 147px;" src="'+e.target.result+'"/></a>';
          };
          reader.readAsDataURL(fileInput.files[0]);
        }
      }
    }
// end fungsi gambar show di bagian tambah data


// fungsi untuk menampilkan gambar sebelum diupload di bagian edit data
function fileValidations(){
  $('#gamb').remove();
  var fileInput = document.getElementById('files');
  var filePath = fileInput.value;
  var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
  if(!allowedExtensions.exec(filePath)){
    swal({
      icon: 'error',
      title: 'Ekstensi Salah',
      text: 'Gunakan Ekstensi .jpeg/.jpg/.png/.gif only.'
    });
    fileInput.value = '';
    return false;
  }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            document.getElementById('imagePreviews').innerHTML = '<a id="gamb" href="'+e.target.result+'" data-fancybox><img class="gim" style="max-height: 147px;" src="'+e.target.result+'"/></a>';
          };
          reader.readAsDataURL(fileInput.files[0]);
        }
      }
    }
// end fungsi gambar show di bagian edit data

// awalan
$(document).ready(function () {
  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

  $(".tutup").on('click',function() {
    document.getElementById("sample_form").reset();
    document.getElementById("form_lelang").reset();
    $("#gimbir").remove();
    $("#files").val('');
  });

// stop lelang
$(document).on('click','.stop-modal',function() {
  $('.dnama').text($(this).data('nama'));
  $('.did_lel').text($(this).data('id_lelang'));
  $('.did_bar').text($(this).data('id_barang'));
  $('#stopModal').modal('show');
});

$('.modal-footer').on('click','.kunci',function(e) {
  e.preventDefault();
  $.ajax({
    type: 'post',
    url: '/stop_lelang',
    data: {
            'id_lelang': $('.did_lel').text(),
            'id_barang': $('.did_bar').text()
          },
    success: function(data) {
      swal({
        icon: 'success',
        title: 'Sukses',
        text: 'Lelang Berhasil Ditutup'
      });
      var angka = data.harga_awal;
      var reverse = angka.toString().split('').reverse().join(''),
      ribuan = reverse.match(/\d{1,3}/g);
      harga = ribuan.join('.').split('').reverse().join('');

      var namabulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
        var tgl = new Date(data.tgl_mulai_lelang);
        var hari = tgl.getDay();
        var tanggal = tgl.getDate();
        var bulan = tgl.getMonth();
        var tahun = tgl.getFullYear();

        $tgl_mulai_lelang = tanggal + " " + namabulan[bulan] + " " + tahun +". Jam "+tgl.getHours()+":"+tgl.getMinutes();

      $('.item'+data.id_barang+"").replaceWith(
        "<tr class='item"+data.id_barang+"'>"+
        "<td>"+data.nama_barang+"</td>"+
        "<td>Rp. "+harga+"</td>"+
        "<td class='text-center'>"+$tgl_mulai_lelang+"</td>"+
        "<td style='text-align: center;'><button type='button' style='cursor: default;' class='btn btn-danger btn-sm lev'>"+data.status+"</button></td>"+
        "<td style='text-align: center;'>"+

        " <button class='btn btn-outline-danger btn-sm btn-ling' style='cursor: default;'><i class='fas fa-lock'></i></button>"+
        
        " <button class='btn btn-outline-dark btn-sm btn-ling info-modal' data-id='"+data.id_barang+"' data-nama='"+data.nama_barang+"' data-harga_awal='"+harga+"' data-harga_akhir='0' data-deskripsi='"+data.deskripsi_barang+"' data-gambar='"+data.foto_barang+"' data-status='"+data.status+"' data-tgl_mulai='"+data.tgl_mulai_lelang+"' data-tgl_selesai='"+data.tgl_selesai_lelang+"'>"+
        "<i class='fas fa-info'></i>"+
        "</button>"+
        
        " <button class='btn btn-outline-danger btn-sm btn-ling delete-modal' data-id='"+data.id_barang+"' data-name='"+data.nama_barang+"'>"+
        "<i class='fas fa-trash-alt'></i>"+
        "</button>"+
        
        "</td>"+
        "</tr>");
    }
  });
});
// end stop lelang

// start lelang
$(document).on('click', '.lelang-modal', function() {
  $('.bar').val($(this).data('nama'));
  $('#id_bar').val($(this).data('id_barang'));
  $('#id_lel').val($(this).data('id_lelang'));
  $('#aksi_lelang').modal('show');
});

$('.modal-footer').on('click','.save_lelang',function(e) {
  e.preventDefault();
  $.ajax({
    type: 'post',
    url: '/mulai_lelang',
    data:  new FormData($('#form_lelang')[0]),
    dataType: "json",
    processData: false,
    contentType: false,
    error:function(){
      swal({
        icon: 'error',
        title: 'Error',
        text: 'Data Harus Diisi'
      });
    },
    success: function(data) {
      if (data == 0) {
       swal({
        icon: 'error',
        title: 'Terjadi Kesalahan',
        text: 'Tgl Mulai Tidak Boleh Melebihi Tgl Selesai'
        });
      }
      else{
        swal({
          icon: 'success',
          title: 'Sukses',
          text: 'Lelang Dimulai'
        });
        var angka = data.harga_awal;
        var reverse = angka.toString().split('').reverse().join(''),
        ribuan = reverse.match(/\d{1,3}/g);
        harga = ribuan.join('.').split('').reverse().join('');

        var namabulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
        var tgl = new Date(data.tgl_mulai_lelang);
        var hari = tgl.getDay();
        var tanggal = tgl.getDate();
        var bulan = tgl.getMonth();
        var tahun = tgl.getFullYear();

        $tgl_mulai_lelang = tanggal + " " + namabulan[bulan] + " " + tahun +". Jam "+tgl.getHours()+":"+tgl.getMinutes();

        $('.item'+data.id_barang+"").replaceWith(
          "<tr class='item"+data.id_barang+"'>"+
          "<td>"+data.nama_barang+"</td>"+
          "<td>Rp. "+harga+"</td>"+
          "<td class='text-center'>"+$tgl_mulai_lelang+"</td>"+
          "<td style='text-align: center;'> <button type='button' style='cursor: default;' class='btn btn-success btn-sm lev'>"+data.status+"</button></td>"+
          "<td style='text-align: center;'>"+

          " <button class='btn btn-success btn-sm btn-ling' style='cursor: default;'><i class='fas fa-pause'></i></button>"+
          
          " <button class='btn btn-outline-dark btn-sm btn-ling info-modal' data-id='"+data.id_barang+"' data-nama='"+data.nama_barang+"' data-harga_awal='"+harga+"' data-harga_akhir='0' data-deskripsi='"+data.deskripsi_barang+"' data-gambar='"+data.foto_barang+"' data-status='"+data.status+"' data-tgl_mulai='"+data.tgl_mulai_lelang+"' data-tgl_selesai='"+data.tgl_selesai_lelang+"'>"+
          "<i class='fas fa-info'></i>"+
          "</button>"+
          
          " <button class='btn btn-outline-danger btn-sm btn-ling delete-modal' data-id='"+data.id_barang+"' data-name='"+data.nama_barang+"'>"+
          "<i class='fas fa-trash-alt'></i>"+
          "</button>"+
          
          "</td>"+
          "</tr>"
        );
      }
    }
  });
});
// end start lelang


// Detail data
$(document).on('click', '.info-modal', function() {
  $('.fancy').attr("href", "/foto/barang/"+$(this).data('gambar')+"");
  $('.f').attr("src", "/foto/barang/"+$(this).data('gambar')+"");
  $('.des').html($(this).data('deskripsi'));
  $('.jeneng').text($(this).data('nama'));
  $('.rego_awal').text('Rp. '+$(this).data('harga_awal'));
  $('.rego_akhir').text('Rp. '+$(this).data('harga_akhir'));
  if ($(this).data('tgl_mulai') == '' || $(this).data('tgl_mulai') == null) {
    $('.durasi').text('-');
  } else {
    var namabulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
    var tgl = new Date($(this).data('tgl_mulai'));
    var tanggal = tgl.getDate();
    var bulan = tgl.getMonth();
    var tahun = tgl.getFullYear();

    var bar = new Date($(this).data('tgl_selesai'));
    var btanggal = bar.getDate();
    var bbulan = bar.getMonth();
    var btahun = bar.getFullYear();
    $tgl_mulai_lelang = tanggal + " " + namabulan[bulan] + " " + tahun;
    $tgl_selesai_lelang = btanggal + " " + namabulan[bbulan] + " " + btahun;
    $('.durasi').text($tgl_mulai_lelang+" s/d "+$tgl_selesai_lelang);
  }
  $('.sta').text($(this).data('status'));
  $('#infoModal').modal('show');
});
// end Detail

//create data
$("#m").on('click',function(e) {
  e.preventDefault();
  $.ajax({
    url:'/store_barang',
    type:"POST",
    data:  new FormData($('#sample_form')[0]),
    dataType: "json",
    processData: false,
    contentType: false,

    error:function(){
      swal({
        icon: 'error',
        title: 'Error',
        text: 'Data Harus Diisi'
      });
    },

    success: function(data) {
      var no =document.getElementById("example").rows.length;
      document.getElementById("sample_form").reset();
      $(".dataTables_empty").remove();
      $("#gimbir").remove();
      swal({
        icon: 'success',
        title: 'Sukses',
        text: 'Data Berhasil Ditambahkan'
      });
      var angka = data.harga_awal;
      var reverse = angka.toString().split('').reverse().join(''),
      ribuan = reverse.match(/\d{1,3}/g);
      harga = ribuan.join('.').split('').reverse().join('');
      $('#example').append(
        "<tr class='item"+data.id_barang+"'>"+
        "<td>"+data.nama_barang+"</td>"+
        "<td>"+harga+"</td>"+
        "<td class='text-center'>-</td>"+
        "<td style='text-align: center;'><button type='button' style='cursor: default;' class='btn btn-secondary btn-sm lev'>"+data.status+"</button></td>"+
        "<td style='text-align: center;'>"+
        
        " <button class='btn btn-outline-primary btn-sm btn-ling lelang-modal' data-id_barang='"+data.id_barang+"' data-id_lelang='"+data.id_lelang+"' data-nama='"+data.nama_barang+"'>"+
        "<i class='fas fa-play'></i>"+
        "</button>"+
        
        " <button class='btn btn-outline-dark btn-sm btn-ling info-modal' data-id='"+data.id_barang+"' data-nama='"+data.nama_barang+"' data-harga_awal='"+harga+"' data-harga_akhir='0' data-deskripsi='"+data.deskripsi_barang+"' data-gambar='"+data.foto_barang+"' data-status='"+data.status+"' data-tgl_mulai='"+data.tgl_mulai_lelang+"' data-tgl_selesai='"+data.tgl_selesai_lelang+"'>"+
        "<i class='fas fa-info'></i>"+
        "</button>"+
        
        " <button class='btn btn-outline-primary btn-sm btn-ling edit-modal' data-id='"+data.id_barang+"' data-nama='"+data.nama_barang+"' data-harga='"+harga+"' data-deskripsi='"+data.deskripsi_barang+"' data-gambar='"+data.foto_barang+"'>"+
        "<i class='fas fa-user-edit'></i>"+
        "</button>"+
        
        " <button class='btn btn-outline-danger btn-sm btn-ling delete-modal' data-id='"+data.id_barang+"' data-name='"+data.nama_barang+"'>"+
        "<i class='fas fa-trash-alt'></i>"+
        "</button>"+
        
        "</td>"+
        "</tr>");
    },
  });
});
// end create data

// update data...
$(document).on('click', '.edit-modal', function() {
  $('.id').val($(this).data('id'));
  $('.nama').val($(this).data('nama'));
  $('.harga').val($(this).data('harga'));

  var test = $(this).data('deskripsi');
  $('.deskripsi').val(test.replace(/\s?(<br\s?\/?>)\s?/g, "\r\n"));
  
  $('#gamb').attr("href", "/foto/barang/"+$(this).data('gambar')+"");
  $('#gim').attr("src", "/foto/barang/"+$(this).data('gambar')+"");
  $('#edit').modal('show');
});

$('.modal-footer').on('click','.edit',function(e) {
  e.preventDefault();
  $.ajax({
    type: 'post',
    url: '/update_barang',
    data:  new FormData($('#form_edit')[0]),
    dataType: "json",
    processData: false,
    contentType: false,
    error:function(){
      swal({
        icon: 'error',
        title: 'Error',
        text: 'Data Harus Diisi'
      });
    },
    success: function(data) {
      swal({
        icon: 'success',
        title: 'Sukses',
        text: 'Data Berhasil Diubah'
      });
      var angka = data.harga_awal;
      var reverse = angka.toString().split('').reverse().join(''),
      ribuan = reverse.match(/\d{1,3}/g);
      harga = ribuan.join('.').split('').reverse().join('');

      $('.item'+data.id_barang+"").replaceWith(
        "<tr class='item"+data.id_barang+"'>"+
        "<td>"+data.nama_barang+"</td>"+
        "<td>Rp. "+harga+"</td>"+
        "<td class='text-center'>-</td>"+
        "<td style='text-align: center;'> <button type='button' style='cursor: default;' class='btn btn-secondary btn-sm lev'>"+data.status+"</button></td>"+
        "<td style='text-align: center;'>"+

        " <button class='btn btn-outline-primary btn-sm btn-ling lelang-modal' data-id_barang='"+data.id_barang+"' data-id_lelang='"+data.id_lelang+"' data-nama='"+data.nama_barang+"'><i class='fas fa-play'></i></button>"+
        
        " <button class='btn btn-outline-dark btn-sm btn-ling info-modal' data-id='"+data.id_barang+"' data-nama='"+data.nama_barang+"' data-harga_awal='"+harga+"' data-harga_akhir='0' data-deskripsi='"+data.deskripsi_barang+"' data-gambar='"+data.foto_barang+"' data-status='"+data.status+"' data-tgl_mulai='"+data.tgl_mulai_lelang+"' data-tgl_selesai='"+data.tgl_selesai_lelang+"'>"+
        "<i class='fas fa-info'></i>"+
        "</button>"+

        " <button class='btn btn-outline-primary btn-sm btn-ling edit-modal' data-id='"+data.id_barang+"' data-nama='"+data.nama_barang+"' data-harga='"+harga+"' data-deskripsi='"+data.deskripsi_barang+"' data-gambar='"+data.foto_barang+"'>"+
        "<i class='fas fa-user-edit'></i>"+
        "</button>"+
        
        " <button class='btn btn-outline-danger btn-sm btn-ling delete-modal' data-id='"+data.id_barang+"' data-name='"+data.nama_barang+"'>"+
        "<i class='fas fa-trash-alt'></i>"+
        "</button>"+
        
        "</td>"+
        "</tr>");
    }
  });
});

// end update data

// hapus data
$(document).on('click', '.delete-modal', function() {
  $('.did').text($(this).data('id'));
  $('.dname').html($(this).data('name'));
  $('#myModal').modal('show');
});
$('.modal-footer').on('click', '.delete',function(e) {
  e.preventDefault();
  $.ajax({
    type: 'post',
    url: '/delete_barang',
    data: {
      '_token': $('input[name=_token]').val(),
      'id_barang': $('.did').text()
    },
    success: function(data) {
      swal({
        icon: 'success',
        title: 'Sukses',
        text: 'Data Berhasil Dihapus'
      });
      $('.item' + $('.did').text()).remove();
    }
  });
});
// end hapus data

});