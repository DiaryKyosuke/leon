            $('#dismiss, .overlay').on('click', function () {
              $('#sidebar').removeClass('active');
              $('#dismiss').removeClass('active');
              $('.overlay').removeClass('active');

            });
            $('#sidebarCollapse').on('click', function () {
             $('#sidebar').toggleClass('active');
             $('#dismiss').toggleClass('active');
             $('.overlay').toggleClass('active');

           });
            $(document).ready(function () {
             var table = $('#example').DataTable( {
              rowReorder: {
               selector: 'td:nth-child(2)'
             },
             responsive: true,
             "pagingType": "full_numbers",
           // "pageLength": 5,
           language: {
             oPaginate: {
              sNext: '<i class="fas fa-chevron-right"></i>',
              sPrevious: '<i class="fas fa-chevron-left"></i>',
              sFirst: '<i class="fas fa-angle-double-left"></i>',
              sLast: '<i class="fas fa-angle-double-right"></i>'
            }
          }  
        } );
        // set form search
        var cari = document.getElementsByTagName("input");
        cari[0].setAttribute('size','10');
        // end

        // set size pagination
        $('.col-md-7').addClass('pagination-sm');
        // end
      });