// start       
$(document).ready(function () {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('.lihat').click(function(){
		if($(this).is(':checked')){
			$('.pw').attr('type','text');
		}else{
			$('.pw').attr('type','password');
		}
	});

//start create data
$("#m").on('click',function(e) {

	e.preventDefault();

	var password = $("#pas").val();
	var confirmPassword = $("#kon").val();

	if (password != confirmPassword) {
		swal({
			icon: 'error',
			title: 'Oops...',
			text: 'Sandi Yang Anda Masukkan Tidak Cocok!'
		});
		return false;
	} 
	$.ajax({
		url:'/store_user',
		type:"POST",
		data: {
			'nama'     : $('input[name="nama"]').val(),
			'email'    : $('input[name="email"]').val(),
			'telp'     : $('input[name="telp"]').val(),
			'username' : $('input[name="username"]').val(),
			'password' : $('input[name="password"]').val(),
		},
		dataType: "json",

		error:function(){
			swal({
				icon: 'error',
				title: 'Error',
				text: 'Data Harus Diisi'
			});
		},

		success: function(data) {
			$(".dataTables_empty").remove();
			var no =document.getElementById("example").rows.length;
			document.getElementById("sample_form").reset();
			$('.pw').attr('type','password');

			swal({
				icon: 'success',
				title: 'Sukses',
				text: 'Data Berhasil Ditambahkan'
			});
			if (data.id_level == 1) {
				$level = "Administrator";
				$w = "primary";
			} else {
				$level = "Petugas";
				$w = "success";
			}
			$('#example').append(
				"<tr class='item"+ data.id +"''>"+
				"<td>"+ data.nama +"</td>"+
				"<td>"+ data.email +"</td>"+
				"<td>"+ data.telp +"</td>"+
				"<td>"+ data.username +"</td>"+
				"<td>"+
				"<button class='btn btn-outline-primary btn-sm btn-ling edit-modal' data-id='"+data.id+"' " +
				"data-telp='"+data.telp+"' data-nama='"+data.nama+"' data-email='"+data.email+"' data-username='"+data.username+"'>"+
				"<i class='fas fa-user-edit'></i>"+
				"</button>"+
				" <button class='btn btn-outline-danger btn-sm btn-ling delete-modal' data-id='"+data.id+"' data-nama='"+data.nama+"'>"+
				"<i class='fas fa-trash-alt'></i>"+
				"</button>"+
				"</td>"+
				"</tr>");
		},
	});
});
// end create data

$(document).on('click', '.edit-modal',function() {
	$('.id').val($(this).data('id'));
	$('.nama').val($(this).data('nama'));
	$('.email').val($(this).data('email'));
	$('.telp').val($(this).data('telp'));
	$('.username').val($(this).data('username'));
	$('#edit').modal('show');
});

$('.modal-footer').on('click','.edit',function(e) {
  e.preventDefault();
	$.ajax({
		type: 'post',
		url: '/update_user',
		data: {
			'_token'    : $('input[name=_token]').val(),
			'id'		: $('.id').val(),
			'nama'      : $('.nama').val(),
			'email'     : $('.email').val(),
			'telp'    	: $('.telp').val(),
			'username'  : $('.username').val()
		},
		dataType: "json",
		success: function(data) {
			swal({
				icon: 'success',
				title: 'Sukses',
				text: 'Data Berhasil Diubah'
			});

			$('.item' + data.id).replaceWith(
				"<tr class='item"+ data.id +"''>"+
				"<td>"+ data.nama +"</td>"+
				"<td>"+ data.email +"</td>"+
				"<td>"+ data.telp +"</td>"+
				"<td>"+ data.username +"</td>"+
				"<td>"+
				"<button class='btn btn-outline-primary btn-sm btn-ling edit-modal' data-id='"+data.id+"' " +
				"data-telp='"+data.telp+"' data-nama='"+data.nama+"' data-email='"+data.email+"' data-username='"+data.username+"'>"+
				"<i class='fas fa-user-edit'></i>"+
				"</button>"+
				" <button class='btn btn-outline-danger btn-sm btn-ling delete-modal' data-id='"+data.id+"' data-nama='"+data.nama+"'>"+
				"<i class='fas fa-trash-alt'></i>"+
				"</button>"+
				"</td>"+
				"</tr>");

		}
	});
});

// end edit data

// delete data
$(document).on('click', '.delete-modal', function() {
	$('.did').text($(this).data('id'));
	$('.dname').html($(this).data('nama'));
	$('#myModal').modal('show');
});
$('.modal-footer').on('click', '.delete',function(e) {
  e.preventDefault();
	$.ajax({
		type: 'post',
		url: '/delete_user',
		data: {
			'_token': $('input[name=_token]').val(),
			'id': $('.did').text()
		},
		success: function(data) {
			swal({
				icon: 'success',
				title: 'Sukses',
				text: 'Data Berhasil Dihapus'
			});
			$('.item' + $('.did').text()).remove();
		}
	});
});
// end delete data


});