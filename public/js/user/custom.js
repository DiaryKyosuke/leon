      window.addEventListener('load', function () {
        $("#preloader").fadeOut("slow");
      });
      // When the user scrolls the page, execute myFunction
      window.onscroll = function() {myFunction()};
      var navbar = document.getElementById("navbar");

      // Get the offset position of the navbar
      var sticky = navbar.offsetTop;

      // Add the sticky class to the navbar when you reach its scroll position. Remove "sticky" when you leave the scroll position
      function myFunction() {
        if (window.pageYOffset >= sticky) {
          navbar.classList.add("sticky")
        } else {
          navbar.classList.remove("sticky");
        }
      };
      $('#x, .overlay').on('click', function () {
       $('#sidebar').removeClass('active');
       $('#x').removeClass('active');
       $('.overlay').removeClass('active');

     });
      $('#sidebarCollapse').on('click', function () {
       $('#sidebar').toggleClass('active');
       $('#x').toggleClass('active');
       $('.overlay').toggleClass('active');

     });

      $(document).ready(function () {
       var table = $('#example').DataTable( {
        rowReorder: {
         selector: 'td:nth-child(2)'
       },
       responsive: true,
       "pagingType": "full_numbers",
           // "pageLength": 5,
           language: {
           	oPaginate: {
           		sNext: '<i class="fas fa-chevron-right"></i>',
           		sPrevious: '<i class="fas fa-chevron-left"></i>',
           		sFirst: '<i class="fas fa-angle-double-left"></i>',
           		sLast: '<i class="fas fa-angle-double-right"></i>'
           	}
           }  
         } );
        // set form search
        var cari = document.getElementsByTagName("input");
        cari[0].setAttribute('size','10');
        // end

        // set size pagination
        $('.col-md-7').addClass('pagination-sm');
        // end
      });
      var wow = new WOW ({
        offset:       75,         
        mobile:       false,       
      });
      wow.init();


// profile js
  $('.show').click(function(){
    if($(this).is(':checked')){
      $('.pw').attr('type','text');
    }else{
      $('.pw').attr('type','password');
    }
  });

  $("#submit").click(function () {
    var password = $("#baru").val();
    var confirmPassword = $("#Konfirmasi").val();
    if (password != confirmPassword) {
      swal({
        icon: 'error',
        title: 'Oops...',
        text: 'Sandi Konfirmasi Tidak Cocok!'
      });
      return false;
    }
    return true;
  });