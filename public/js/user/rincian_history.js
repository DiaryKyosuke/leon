	setInterval(function () {
 		$(".coment").load(location.href + " #refresh");
 	},100);
	window.setTimeout("waktu()", 100);
	function waktu() {
		var waktu = new Date();
		setTimeout("waktu()", 100);
		jam = waktu.getHours();
		menit = waktu.getMinutes();
		detik = waktu.getSeconds();
		document.getElementById("jam").innerHTML = ("0" + jam).slice(-2);
		document.getElementById("menit").innerHTML = ("0" + menit).slice(-2);
		document.getElementById("detik").innerHTML =  ("0" + detik).slice(-2);
	}
	$(document).ready(function() {
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
 		// estimasi waktu dibuka lelang
 		var x = setInterval(function () {
 			var dates = $("#tgl_selesai").val();
 			var deadline    = new Date(dates).getTime();
 			var waktu       = new Date().getTime();
 			var distance    = deadline - waktu;

 			var days    = Math.floor((distance / (1000*60*60*24)));
 			var hours   = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
 			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
 			var seconds = Math.floor((distance % (1000 * 60)) / 1000);
 			if (distance < 0) {
 				$.ajax({
 					type: 'post',
 					url: '/status_ditutup',
 					data: {
 						'id_lelang': $('#id_lel').val(),
 						'id_barang': $('#id_barang').val()
 					},
 					success: function(data) {
 						console.log(data);
 						clearInterval(x);
 						$('#taw_barang').remove();
 						$('.testing').remove();
 						document.getElementById("iki_button").innerHTML = "<button style='cursor:default !important;' class='btn btn-danger btn-med btn-block'><i class='fas fa-times-circle'></i><span> Lelang Yang Anda Ikuti Sudah Tutup</span></button>";
 					}
 				});
 			}
 		}, 100);
 		// end estimasi

 		var months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agust', 'Sep', 'Okt', 'Nov', 'Des'];
 		var date = new Date();
 		var day = date.getDate();
 		var month = date.getMonth();
 		var yy = date.getYear();
 		var year = (yy < 1000) ? yy + 1900 : yy;
 		$dates = (day + ' ' + months[month] + ' ' + year);
 		document.getElementById("date").innerHTML = $dates;

 		var namabulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
 		var da = $('#tgl_awal').val();
 		var tgl = new Date(da);
 		var tanggal = tgl.getDate();
 		var bulan = tgl.getMonth();
 		var tahun = tgl.getFullYear();
 		$tgl_mulai_lelang = tanggal + " " + namabulan[bulan] + " " + tahun;
 		$('.mulai').text($tgl_mulai_lelang+", Jam "+("0" + tgl.getHours()).slice(-2)+":"+("0" + tgl.getMinutes()).slice(-2));

 		var dates = $('#tgl_selesai').val();
 		var bar = new Date(dates);
 		var btanggal = bar.getDate();
 		var bbulan = bar.getMonth();
 		var btahun = bar.getFullYear();
 		$tgl_selesai_lelang = btanggal + " " + namabulan[bbulan] + " " + btahun;
 		$('.selesai').text($tgl_selesai_lelang+", Jam "+("0" + bar.getHours()).slice(-2)+":"+("0" + bar.getMinutes()).slice(-2));

 		$('#tawr').on('click',function(e) {
 			e.preventDefault();
 			$.ajax({
 				type: 'post',
 				url: 'tawar_barang',
 				data:  new FormData($('#taw_barang')[0]),
 				dataType: "json",
 				processData: false,
 				contentType: false,
 				success: function(data) {
 					if (data[0] == "error") {
 						var angka = data[1];
 						var reverse = angka.toString().split('').reverse().join(''),
 						ribuan = reverse.match(/\d{1,3}/g);
 						harga = ribuan.join('.').split('').reverse().join('');
 						swal({
 							icon: 'warning',
 							title: 'Gagal!!',
 							text: 'Nominal Harus Diatas Rp. '+harga+''
 						});
 					}else{
 					document.getElementById("taw_barang").reset();
 					}
 				}
 			});
 		});
 	});