<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'guest'], function () {
	// halaman user belum login
	Route::get('/', function () {
		return view('home');
	});
	Route::get('akan', 'user\BerandaController@akan');
	Route::post('/status_barangs', 'user\BerandaController@statusup');

	// halaman register user
	Route::get('daftar', 'sign\RegisterController@index');
	Route::post('tambah', 'sign\RegisterController@create');

// halaman login
	// admin
	Route::get('logadmin','sign\LoginController@logadmin');
	Route::post('melbu', 'sign\LoginController@melbu');
	// user
	Route::get('sign', 'sign\LoginController@index');
	Route::post('masuk', 'sign\LoginController@masuk');
});
// logout admin dan user
Route::get('logout', 'sign\LoginController@logout');

Route::group(['middleware' => 'revalidate'], function()
{
	Route::group(['middleware' => 'auth:admin'], function()
	{
		Route::get('dashboard', 'admin\DashboardController@index');
		Route::get('petugas','admin\DashboardController@petugas');
		Route::post('/store_petugas','admin\DashboardController@store_petugas');
		Route::post('/edit_petugas','admin\DashboardController@edit_petugas');
		Route::post('/delete_petugas','admin\DashboardController@delete_petugas');

		Route::get('user','admin\UserController@index');
		Route::post('/store_user','admin\UserController@store');
		Route::post('/update_user','admin\UserController@update');
		Route::post('/delete_user','admin\UserController@destroy');

		Route::get('barang','admin\BarangController@index');
		Route::post('/store_barang','admin\BarangController@store');
		Route::post('/update_barang','admin\BarangController@update');
		Route::post('/delete_barang','admin\BarangController@destroy');
		Route::post('/mulai_lelang','admin\BarangController@nglelang');
		Route::post('/stop_lelang','admin\BarangController@stop_lelang');

		Route::get('profile','admin\ProfileController@index');
		Route::post('ubah_akun','admin\ProfileController@ubah_akun');
		Route::post('ubah_pw','admin\ProfileController@ubah_pw');

		Route::get('laporan','admin\LaporanController@index');
		Route::get('cetak','admin\LaporanController@cetak_pdf');
	});
	

// halaman setelah user login
	Route::group(['middleware' => 'auth:user'], function()
	{
		Route::get('beranda', 'user\BerandaController@index');
		Route::get('coming', 'user\BerandaController@coming');
		Route::get('running', 'user\BerandaController@running');

		Route::post('/status_barang', 'user\BerandaController@statusup');
		Route::get('info.{id_barang}.{id}', 'user\BerandaController@info');
		Route::post('/ikuti', 'user\BerandaController@ikuti');
		Route::post('tawar_barang', 'user\BerandaController@tawar_barang');
		Route::post('/status_ditutup', 'user\BerandaController@ditutup');

		Route::get('history', 'user\HistoriController@index');
		Route::get('rincian.{id_barang}.{id_user}', 'user\HistoriController@rincian');

		Route::get('profil', 'user\ProfilController@index');
		Route::post('edit_user', 'user\ProfilController@edit_user');
		Route::post('edit_password', 'user\ProfilController@edit_password');
	});


});