@extends('layouts.user.home')
@section('judul','Sedang Berjalan')
@section('pelelangan','aktif')
@section('sedang','on')
@section('lels','active')
@section('content')
<div class="background" id="refresh">
	<div class=" text-center my-3">
		<h2 class="judul">Sedang Berjalan</h2>
		<div class="row mx-auto my-auto">
			<?php
			function limit_words($string, $word_limit){
				$words = explode(" ",$string);
				return implode(" ",array_splice($words,0,$word_limit));
			}
			foreach ($barang as $bar) {
				$long_string = $bar->deskripsi_barang;
				$deskripsi[$nol++] = limit_words($long_string, 15);
			}
			?>
			@foreach($barang as $bar)
			<div class="col-sm-3 mt-3 test{{++$i}}">
				<div class="card card-custom">
					<a class="crop" data-fancybox href="/foto/barang/{{$bar->foto_barang}}">
						<img class="card-img-top img-fluid" src="/foto/barang/{{$bar->foto_barang}}" alt="Card image cap">
					</a>
					<div class="card-body" style="border-top: 5px solid #FA8C42;">
						<h4 class="card-title">{{$bar->nama_barang}}</h4>
						<?php $a = str_replace('<br>', ' ', $deskripsi[$nols++]); ?>
						<p class="card-text text-left">{{$a}}</p>
						<div class="row">
							<div class="col">
								<?php $id = Auth::guard('user')->user()->id; ?>
								<a href="info.{{$bar->id_barang}}.{{$id}}" class="text-left">Ikuti Lelang</a>
							</div>
							<div class="col">
								<p class="text-right" style="font-size: 18px;">Rp. {{number_format($bar->harga_awal, 0, ".", ".")}}</p>
							</div>
						</div>
						<div id="clock">
							<div><span id="days{{$i}}" class="days"></span><p>Hari</p></div>
							<div><span id="hours{{$i}}" class="hours"></span><p>Jam</p></div>
							<div><span id="minutes{{$i}}" class="minutes"></span><p>Menit</p></div>
							<div><span id="seconds{{$i}}" class="seconds"></span><p>Detik</p></div>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" id="tgl{{$i}}" value="{{$bar->tgl_selesai_lelang}}">
			<input type="hidden" name="id_lelang" id="id_lel{{$i}}" value="{{$bar->id_lelang}}">
			<input type="hidden" name="id_barang" id="id_barang{{$i}}" value="{{$bar->id_barang}}">
			@endforeach
			
		</div>
	</div>
	{{ $barang->links() }}
	<br>
</div>
@endsection

@section('waktu')
@foreach ($barang as $bar)
<script type="text/javascript" class="reload">
	
	function animation(span) {
		span.className = "turn seconds";
		setTimeout(function () {
			span.className = "seconds"
		}, 700);
	}
	$(document).ready(function(){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
		var x = setInterval(function () {

			var angka = <?php echo $angka++; ?>;
			var hari    = document.getElementById("days"+angka);
			var jam     = document.getElementById("hours"+angka);
			var menit   = document.getElementById("minutes"+angka);
			var detik   = document.getElementById("seconds"+angka);
			var dates = $("#tgl"+angka).val();
			var deadline    = new Date(dates).getTime();
			var waktu       = new Date().getTime();
			var distance    = deadline - waktu;

			var days    = Math.floor((distance / (1000*60*60*24)));
			var hours   = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);

			if (days < 10)
            {
               days = '0' + days;
            }
            if (hours < 10)
            {
               hours = '0' + hours;
            }
            if (minutes < 10)
            {
               minutes = '0' + minutes;
            }
            if (seconds < 10)
            {
               seconds = '0' + seconds;
            }
			
			hari.innerHTML    = days;
			jam.innerHTML     = hours;
			menit.innerHTML   = minutes;
			detik.innerHTML   = seconds;
            //animation
            animation(detik);
            if (seconds == 00) animation(menit);
            if (minutes == 00 && seconds == 00) animation(jam);
            if (hours == 00 && minutes == 00 && seconds == 00) animation(hari);
            if (distance < 0) {
            	$.ajax({
            		type: 'post',
            		url: '/status_ditutup',
            		data: {
            			'id_lelang': $('#id_lel'+angka).val(),
            			'id_barang': $('#id_barang'+angka).val()
            		},
            		success: function(data) {
            			console.log(data);
            		}
            	});
            	$('.test'+angka).remove();
            	$("#refresh").load(location.href + " #refresh");
            }
        }, 1000);
	});
</script>
@endforeach
@endsection