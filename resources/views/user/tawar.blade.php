 @extends('layouts.user.home')
 @section('judul','Mulai Tawar Barang')
 @section('content')
 <div class="container" style="padding-bottom: 100px;">		
 	<div class="row">
 		<div class="col-md-3 mt-3">
 			<div id="accordion" class="tom">
 				<div class="card mb-0">
 					<div class="card-header" data-toggle="collapse" href="#collapseOne">
 						<a class="card-title" style="font-size: 12px;"><b>ESTIMASI WAKTU SERVER</b></a>
 					</div>
 					<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
 						<div class="card-body text-center">
 							<!-- <b>19 Feb 2020, 19:30</b> -->
 							<b class="wektu"><span id="date"></span>, <span id="jam"></span>:<span id="menit"></span>:<span id="detik"></span></b>
 						</div>
 					</div>
 				</div>
 			</div>
 			<div class="mt-3">
 				<a class="crop" data-fancybox href="/foto/barang/{{$barang->foto_barang}}">
 					<img class="card-img-top img-fluid" src="/foto/barang/{{$barang->foto_barang}}" alt="Card image cap">
 				</a>
 			</div>
 		</div>

 		<div class="col mt-3">
 			<h4 class="hc">{{$barang->nama_barang}}</h4>
 			<hr>
 			<div>
 				<p class="availability" style="margin: 0px;">
 					<span style="font-size: 20px;">Harga Awal:</span>
 				</p>
 				<div>
 					<h4 class="hc">Rp. {{number_format($barang->harga_awal, 0, ".", ".")}}</h4>
 				</div>
 			</div>

 			<table class="table table-striped font">
 				<tbody>
 					<tr>
 						<td colspan="2">
 							<?php echo $barang->deskripsi_barang; ?>
 						</td>
 					</tr>
 					<tr>
 						<td>
 							Tanggal Mulai Lelang
 						</td>
 						<td class="mulai"></td>
 					</tr>
 					<tr>
 						<td>
 							Batas Akhir Penawaran
 						</td>
 						<td class="selesai"></td>
 					</tr>
 				</tbody>
 			</table>

 			<div class="col-md-12 col-xs-12" id="iki_button">
 				@if($barang->status == 'Dibuka')

 				@if($status == 'Diikuti')
 				<button class="testing nohover btn btn-outline-success btn-med btn-block">
 					<i class="fa fa-check-square"></i>
 					<span> Di Ikuti</span>
 				</button>
 				@else
 				<button id="ikut" class="testing btn btn-primary btn-med btn-block ikuti" data-id_barang="{{$barang->id_barang}}" data-id_user="{{$users->id}}">
 					<i class="fa fa-check-square"></i>
 					<span>Ikut Lelang</span>
 				</button>
 				@endif

 				@else
 				<button style="cursor:default !important;" class="btn btn-danger btn-med btn-block">
 					<i class="fas fa-times-circle"></i>
 					<span> Lelang Sudah Tutup</span>
 				</button>
 				@endif
 			</div>
 		</div>
 	</div>
 	<!-- if jika mengikuti lelang -->
 	<div class="row mt-4 d-none" id="taw">
 		<div class="col-md-4 mt-3">
 			@if($barang->status !== 'Ditutup')
 			<form id="taw_barang">
 				@csrf
 				<div class="form-row align-items-center">
 					<div class="col">
 						<div class="input-group input-group-sm">
 							<div class="input-group-prepend">
 								<div class="input-group-text">Rp. </div>
 							</div>
 							<input type="hidden" name="id_lelang" value="{{$barang->id_lelang}}">
 							<input type="hidden" name="id_barang" value="{{$barang->id_barang}}">
 							<input type="hidden" name="id_user" value="{{$users->id}}">
 							<input name="tawars" data-mask="000.000.000" data-mask-reverse="true" type="text"class="form-control form-control-sm" placeholder="Tawaran Anda" style="z-index: 0;">
 						</div>
 					</div>
 					<div class="col-auto my-1">
 						<button type="submit" class="btn btn-primary btn-sm" id="tawr">Tawar</button>
 					</div>
 				</div>
 			</form>
 			@endif
 		</div>
 		<div class="col mt-3 coment">
 			<div id="refresh">
 				@foreach($tawar as $t)
 				<div class="media border p-3 mb-2 mt-3">
 					<img src="{{url('foto/user6.png')}}" alt="foto-user" class="mr-3 mt-1 rounded-circle" style="width:60px;">
 					<div class="media-body">
 						<div class="row">
 							<div class="col">
 								<font class="post" size="4px;"><b>{{$t->nama}}</b> <small> Posted on <i>{{$t->created_at}}</i></small></font>
 								<br>
 								<font class="tawaran" size="3px;">Rp. {{number_format($t->penawaran_harga, 0, ".", ".")}}</font>
 							</div>
 						</div>
 					</div>
 				</div>
 				@endforeach
 			</div>
 		</div>
 	</div>
 	<!-- end if -->
 </div>
 <input type="hidden" name="" id="tgl_awal" value="{{$barang->tgl_mulai_lelang}}">
 <input type="hidden" name="" id="tgl_selesai" value="{{$barang->tgl_selesai_lelang}}">
 <input type="hidden" name="id_lelang" id="id_lel" value="{{$barang->id_lelang}}">
 <input type="hidden" name="id_barang" id="id_barang" value="{{$barang->id_barang}}">
 @endsection
 @section('waktu')
 <script type="text/javascript" src="{{url('js/user/tawar.js')}}"></script>
 <script type="text/javascript">
 	$(document).ready(function() {
 		var ikut = "{{$status}}";
 		if (ikut == "Diikuti") {
 			$("#taw").removeClass("d-none");
 		}
 	});
 </script>

 @endsection