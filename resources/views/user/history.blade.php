@extends('layouts.user.home')
@section('judul','History')
@section('pelelangan','aktif')
@section('history','on')
@section('lels','active')
@section('content')
<style type="text/css">
	.lev{
		border-radius: 16px; 
		font-size: 12px;
	}
	.card-font{
        font-size: 15px;
    }
</style>
<div class="card mt-3">
	<div class="card-header">
		<div class="row">
			<div class="col">
				History Lelang
			</div>
		</div>
	</div>
	<div class="card-body card-font">
		<!-- content -->
		<table id="example" class="table table-striped table-bordered table-hover" style="width: 100%;">
			<thead>
				<tr style="text-align: center;">
					<th style="width: 5px;">No.</th>
					<th style="width: 150px;">Nama Barang</th>
					<th style="width: 160px;">Pemenang</th>
					<th style="width: 80px;">Hrg. Awal</th>
					<th style="width: 80px;">Hrg. Akhir</th>
					<th style="width: 10px;">Status</th>
					<th style="width: 10px;">Rincian</th>
				</tr>
			</thead>
			<tbody>
				@foreach($barang as $b)
				<tr>
					<td style="text-align: center;">{{++$i}}</td>
					<td>{{$b->nama_barang}}</td>
					<td style="text-align: center;"> 
						@if($b->id_user == null)
						<button type="button" style="cursor: default;" class="btn btn-danger btn-sm lev">Tidak Ada Pemenang</button>
						@else
						<?php if ($b->id_user !== $users->id): ?>
							<?php $data = $nama->where('id','=',$b->id_user)->first(); ?>
							<button type="button" style="cursor: default;" class="btn btn-info btn-sm lev">{{$data->nama}}</button>
						<?php else: ?>
							<button type="button" style="cursor: default;" class="btn btn-primary btn-sm lev">Anda Pemenang Lelang</button>
						<?php endif ?>
						@endif
					</td>
					<td style="text-align: center;">Rp. {{number_format($b->harga_awal, 0, ".", ".")}}</td>
					<td style="text-align: center;">Rp. {{number_format($b->harga_akhir, 0, ".", ".")}}</td>
					<td style="text-align: center;">
						@if($b->status == 'Ditutup')
						<button type="button" style="cursor: default;" class="btn btn-danger btn-sm lev">Lelang {{$b->status}}</button>
						@else
						<button type="button" style="cursor: default;" class="btn btn-primary btn-sm lev">Lelang {{$b->status}}</button>
						@endif
					</td>
					<?php $id_user = Auth::guard('user')->user()->id; ?>
					<td style="text-align: center;"><a href="rincian.{{$b->id_barang}}.{{$id_user}}" style="cursor: default;" class="btn btn-dark btn-sm lev">Detail</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection