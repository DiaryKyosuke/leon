@extends('layouts.user.home')
@section('judul','Profile Anda')
@section('akuns','active')
@section('content')
<div class="row mt-3 justify-content-md-center">
	
	<div class="col-md-5">
		<div class="card shadow edit">
			<div class="card-body">
				<h5 class="card-title">Pengaturan Akun</h5>
				<form method="post" action="edit_user">
					@csrf
					<div class="form-group">
						<input class="id" type="hidden" name="id" value="{{$data->id}}" required>

						<label>Nama Lengkap</label>
						<input type="text" class="form-control form-control-sm" name="nama" value="{{$data->nama}}" required>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label>Email Aktif</label>
							<input type="text" class="form-control form-control-sm" name="email" value="{{$data->email}}" required>
						</div>
						<div class="form-group col-md-6">
							<label>No. HP / W.A</label>
							<input type="text" class="form-control form-control-sm" name="telp" value="{{$data->telp}}" required>
						</div>
					</div>
					<hr>
					<div class="form-group row float-right">
						<div class="col">
							<button type="submit" class="btn btn-primary btn-sm shadow">Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="card shadow edit">
			<div class="card-body">
				<h5 class="card-title">Ganti Password</h5>
				<form method="post" action="edit_password">
					@csrf
					<div class="form-group">
						<input type="hidden" name="id" value="{{$data->id}}" required>

						<label>Password Lama</label>
						<input type="password" id="lama" class="pw form-control form-control-sm" name="pw_lama" required>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label>Password Baru</label>
							<input type="password" id="baru" class="pw form-control form-control-sm" name="pw_baru" required>
						</div>
						<div class="form-group col-md-6">
							<label>Konfirmasi Password</label>
							<input type="password" id="Konfirmasi" class="pw form-control form-control-sm" name="konfirmasi_pw" required>
						</div>
					</div>
					<div class="form-group">
						<div class="custom-control custom-checkbox mb-3">
							<input type="checkbox" class="custom-control-input show" id="customCheck1">
							<label class="custom-control-label" for="customCheck1" style="top: 25rem;">Lihat password</label>
						</div>
					</div>
					<hr>
					<div class="form-group row float-right">
						<div class="col">
							<button id="submit" type="submit" class="btn btn-primary btn-sm shadow">Simpan Perubahan</button>
							<button type="reset" class="btn btn-danger btn-sm shadow">Reset</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
@endsection
