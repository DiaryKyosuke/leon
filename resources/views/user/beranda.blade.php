@extends('layouts.user.home')
@section('judul','Beranda')
@section('beranda','aktif')
@section('ber','active')
@section('content')
<!-- jumbotron -->
<div class="jumbotron jumbotron-fluid">
	<div class="container" style="margin-top: -60px;">
		<h1 class="wow animated fadeInDown">Selamat datang di Pelelangan Online!<br><font style="font-size: 30px;">Lelang mudah, aman, unggul, dan terpercaya.</font></h1>
	</div>
</div>
<!-- container -->
<div id="body" class="container">
	<!-- deskripsi -->
	<div class="row deskripsi">

		<div class="row">
			<div class="col-lg  wow animated fadeInLeft">
				<img src="{{url('/foto/ui/lelang.png')}}" class="img-fluid">
			</div>
			<div class="col-lg wow animated fadeInRight">
				<h3>Pelaksanaan <span>lelang</span> sangat <span>mudah</span></h3>
				<p>Lelang menjadi lebih praktis karena dapat menawar barang dari mana saja, tanpa perlu hadir langsung.</p>
				<hr>
			</div>
		</div>
		<div class="m-5"></div>

		<div class="row">
			<div class="col-lg wow animated fadeInUp gambar">
				<img src="{{url('/foto/ui/terpercaya.png')}}" class="img-fluid">
				<hr>
			</div>
			<div class="col-lg wow animated fadeInDown">
				<h3>Lelang <span>terpercaya</span> dan <span>kompetitif</span></h3>
				<p>Semua barang yang dilelang sudah dicek keasliannya. Cara penawaran lelang yang khas, tidak ada prioritas dan pembatasan peserta lelang.</p>
				<hr>
			</div>
			<div class="col-lg wow animated fadeInUp gambars">
				<img src="{{url('/foto/ui/terpercaya.png')}}" class="img-fluid">
				<hr>
			</div>
		</div>
		<div class="m-5"></div>

		<div class="row">
			<div class="col-lg wow animated fadeInUp">
				<img src="{{url('/foto/ui/aman.png')}}" class="img-fluid">
			</div>
			<div class="col-lg wow animated fadeInDown">
				<h3>Proses lelang yang <span>aman</span> dan <span>terjadwal</span></h3>
				<p>Pelelangan dipimpin dan dilaksanakan oleh petugas. Jatuh tempo / waktu dari barang yang dilelang sudah ditentukan dan disetujui pelelang.</p>
				<hr>
			</div>
		</div>
		<div class="m-4"></div>
	</div>
	<!-- end deskripsi -->
</div>

<!-- end container -->
@endsection