@extends('layouts.sign.masuk')
@section('judul','Login')
@section('content')
<div class="row">
	<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
		<div class="card card-signin my-5">
			<div class="card-body">
				<h5 class="card-title text-center">Sign In</h5>
				<form class="form-signin" method="post" action="melbu">
					@csrf
					<div class="form-label-group">
						<input type="text" class="form-control" placeholder="Username" name="username" required>
					</div>

					<div class="form-label-group">
						<input type="password" id="pw" name="password" class="form-control" placeholder="Password" required>
					</div>

					<div class="custom-control custom-checkbox mb-3">
						<input type="checkbox" class="custom-control-input show" id="customCheck1">
						<label class="custom-control-label" for="customCheck1">Lihat password</label>
					</div>
					<button class="btn btn-sm btn-primary btn-block text-uppercase" type="submit">Sign in</button>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection