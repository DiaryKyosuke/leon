@extends('layouts.sign.daftar')
@section('judul','Registrasi Member')
@section('content')
<div class="card mt-4 mb-4 col-md-7 col-lg-7 mx-auto card-cus">
	<div class="card-header">
		<div class="row">
			<div class="col">
				Register
			</div>
		</div>
	</div>
	<div class="card-body">
		<form class="fomr-cus" method="post" action="tambah">
			@csrf
			<div class="form-row">
				<div class="form-group col-md-6">
					<label>Nama Lengkap</label>
					<input name="nama" type="text" class="form-control form-control-sm" required>
				</div>
				<div class="form-group col-md-6">
					<label>Email</label>
					<input name="email" type="text" class="form-control form-control-sm" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label>Telepon</label>
					<input name="telp" type="text" class="form-control form-control-sm" required>
				</div>
				<div class="form-group col-md-6">
					<label>Username</label>
					<input name="username" type="text" class="form-control form-control-sm" required>
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label>Password</label>
					<input id="pw" name="password" type="password" class="form-control form-control-sm pw" required autocomplete>
				</div>
				<div class="form-group col-md-6">
					<label>Konfirmasi Password</label>
					<input id="kon" name="konfirmasi" type="password" class="form-control form-control-sm pw" required autocomplete>
				</div>
			</div>
			<div class="form-group">
				<div class="custom-control custom-checkbox mb-3">
					<input type="checkbox" class="custom-control-input show" id="customCheck1">
					<label class="custom-control-label" for="customCheck1">Lihat password</label>
				</div>
			</div>
			<button id="submit" type="submit" class="btn btn-primary btn-sm btn-block btn-cus">Sign in</button>
		</form>
	</div>
</div>
@endsection

