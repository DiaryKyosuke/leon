@extends('layouts.admin.home')
@section('judul','Cetak Laporan')
@section('laporan','active')
@section('content')
<div class="row mt-3 justify-content-md-center">
	<div class="col-md-7">
		<div class="card shadow edit">
			<div class="card-body pb-0">
				<h5 class="card-title mb-4">Cetak Laporan</h5>
				<form id="cetak" method="get" action="cetak">
					@csrf
					<div class="form-row">
						<div class="form-group col-md-6">
							<label>Tanggal Awal</label>
							<input type="text" id="awal" class="datepicker form-control form-control-sm" name="awal" required readonly>
						</div>
						<div class="form-group col-md-6">
							<label>Tanggal Akhir</label>
							<input type="text" id="akhir" class="datepicker form-control form-control-sm" name="akhir" required readonly>
						</div>
					</div>
					<span style="font-size: 15px; color: red;">Catatan : Hanya barang yang lelangnya sudah ditutup</span>
					<hr>
					<div class="form-group row float-right">
						<div class="col">
							<button id="submit" type="submit" class="btn btn-primary btn-sm shadow"><i class="fas fa-print"></i> Print</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection
@section('crud')
<script type="text/javascript">
	$(function(){
		$(".datepicker").datepicker({
			format: 'dd-mm-yyyy',
			autoclose: true,
		});
	});
	$(document).ready(function() {
		$("#submit").click(function () {
			var date = $('#awal').val();
			var newdate = date.split("-").reverse().join("-");
			var tgl = new Date(newdate);
			tanggals = ("0" + tgl.getDate()).slice(-2);
			bulan = tgl.getMonth();
			var bul  = bulan + 1;
			$tgl_mulai_lelang = tgl.getFullYear() + "" +bul+ "" + tanggals;

			var dates = $('#akhir').val();
			var newdates = dates.split("-").reverse().join("-");
			var tgls = new Date(newdates);
			tanggals = ("0" + tgls.getDate()).slice(-2);
			bulans = tgls.getMonth();
			var buls  = bulans + 1;
			$tgl_selesai_lelang = tgls.getFullYear() + "" +buls+ "" + tanggals;

			if (date == "" || dates == "") {
				swal({
					icon: 'error',
					title: 'Oops...',
					text: 'Inputan Tidak Boleh Kosong!'
				});
				return false;
			}

			if ($tgl_mulai_lelang > $tgl_selesai_lelang) {
				swal({
					icon: 'error',
					title: 'Oops...',
					text: 'Tgl Awal Tidak Boleh Melebihi Tgl Akhir!'
				});
				return false;
			}else{
				return true;
			}
		});
	});
</script>
@endsection