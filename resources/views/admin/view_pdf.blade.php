<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<table style="width: 100%;">
	<tr>
		<td><img src="foto/logo_pdf.png" style="width: 400px;" alt="IMG"></td>
	</tr>
</table>
	<br><div style="border: 2px solid black;"></div><br>
	<div class="col-md-14">
		<table width="100%">
			<tr>
			<td><h2>Laporan Pelelangan Online</h2></td>
				
			</tr>
			<tr>
				<td>
					<div class="form-group">
						<?php
						$awals = Carbon\Carbon::parse($awal)->formatLocalized('%d %B %Y');
						$akhirs = Carbon\Carbon::parse($akhir)->formatLocalized('%d %B %Y');
						?>
						<strong>Periode : {{$awals}} s/d {{$akhirs}}</strong>
					</div>
				</td>
			</tr>
		</table>
	</div>

	<table class="table table-striped table-bordered">
		<thead class="thead-dark">
			<tr style="text-align: center;">
				<th scope="col">No. </th>
				<th scope="col">Tgl Ditutup</th>
				<th scope="col">Barang</th>
				<th scope="col">Pemenang</th>
				<th scope="col">Email Pemenang</th>
				<th scope="col">Hrg. Awal</th>
				<th scope="col">Hrg. Akhir</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 1; ?>
			@foreach($history as $h)
			<tr style="text-align: center;">
				<th scope="row">{{$i++}}</th>
				<td>{{ Carbon\Carbon::parse($h->tgl_selesai_lelang)->formatLocalized('%d %B %Y')}}</td>
				<td>{{$h->nama_barang}}</td>
				@if($h->id_user == null)
				<td style="background-color: #ff3333; color: white;">
					<span class="" >-</span>
				</td>
				<td>-</td>
				@else
				<td style="background-color: #1778f2; color: white;">
					<?php $data = $users->where('id','=',$h->id_user)->first(); ?>
					<span class="">{{$data->nama}}</span>
				</td>
				<td>{{$data->email}}</td>
				@endif
				<td>Rp. {{number_format($h->harga_awal, 0, ".", ".")}}</td>
				<td>Rp. {{number_format($h->harga_akhir, 0, ".", ".")}}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>