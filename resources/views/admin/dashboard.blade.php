@extends('layouts.admin.home')
@section('judul','Dashboard')
@section('dashboard','active')
@section('content')
<nav aria-label="breadcrumb" class="mt-3">
    <h6>Dashboard</h6>
    <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="#"><i class="fas fa-home"></i> Home</a></li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-3 mt-3">
        <div class="card border-primary mx-sm-1 p-3">
            <div class="card border-primary shadow text-primary p-3 my-card"><span class="fas fa-cube" aria-hidden="true"></span></div>
            <div class="text-primary text-center mt-3"><h6>Barang</h6></div>
            <div class="text-primary text-center mt-2"><h5>{{$barang}}</h5></div>
        </div>
    </div>
    <div class="col-md-3 mt-3">
        <div class="card border-dark mx-sm-1 p-3">
            <div class="card border-dark shadow text-dark p-3 my-card" ><span class="fas fa-truck-loading" aria-hidden="true"></span></div>
            <div class="text-dark text-center mt-3"><h6>Akan Datang</h6></div>
            <div class="text-dark text-center mt-2"><h5>{{$datang}}</h5></div>
        </div>
    </div>
    <div class="col-md-3 mt-3">
        <div class="card border-success mx-sm-1 p-3">
            <div class="card border-success shadow text-success p-3 my-card" ><span class="fas fa-box-open" aria-hidden="true"></span></div>
            <div class="text-success text-center mt-3"><h6>Lelang Dibuka</h6></div>
            <div class="text-success text-center mt-2"><h5>{{$buka}}</h5></div>
        </div>
    </div>
    <div class="col-md-3 mt-3">
        <div class="card border-danger mx-sm-1 p-3">
            <div class="card border-danger shadow text-danger p-3 my-card" ><span class="fas fa-box" aria-hidden="true"></span></div>
            <div class="text-danger text-center mt-3"><h6>Lelang Ditutup</h6></div>
            <div class="text-danger text-center mt-2"><h5>{{$tutup}}</h5></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="card mt-3" style="width: 100%;">
    <div class="card-header">
        <div class="row">
            <div class="col">
                History Lelang
            </div>
        </div>
    </div>
    <div class="card-body card-font">
        <!-- content -->
        <table id="example" class="table table-striped table-bordered table-hover" style="width: 100%;">
            <thead>
                <tr style="text-align: center;">
                    <th style="width: 5px;">No.</th>
                    <th style="width: 130px;">Nama Barang</th>
                    <th style="width: 100px;">Foto</th>
                    <th style="width: 160px;">Pemenang</th>
                    <th style="width: 80px;">Hrg. Awal</th>
                    <th style="width: 80px;">Hrg. Akhir</th>
                </tr>
            </thead>
            <tbody>
                @foreach($barangs as $b)
                <tr>
                    <td style="text-align: center;">{{++$i}}</td>
                    <td>{{$b->nama_barang}}</td>
                    <td style="text-align: center;">
                        <div class="crop">
                            <a data-fancybox href="/foto/barang/{{$b->foto_barang}}">
                                <button class="btn btn-outline-primary btn-sm btn-ling">
                                <i class="fas fa-image"></i>
                            </button>
                            </a>
                        </div>
                    </td>
                    <td style="text-align: center;"> 
                        @if($b->id_user == null)
                            <button type="button" style="cursor: default;" class="btn btn-danger btn-sm lev">Tidak Ada Pemenang</button>
                        @else
                            <?php $data = $users->where('id','=',$b->id_user)->first(); ?>
                            <button type="button" style="cursor: default;" class="btn btn-primary btn-sm lev">{{$data->nama}}</button>
                        @endif
                    </td>
                    <td style="text-align: center;">Rp. {{number_format($b->harga_awal, 0, ".", ".")}}</td>
                    <td style="text-align: center;">Rp. {{number_format($b->harga_akhir, 0, ".", ".")}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</div>
@endsection

