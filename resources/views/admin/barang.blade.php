@extends('layouts.admin.home')
@section('judul','Barang')
@section('barang','active')
@section('content')
<style type="text/css">
	.a{
		border: 2px solid #d9d9d9;
		overflow: hidden; 
		width: 320px; 
		height: 250px;
	}
	.desk{
		border: 2px solid #d9d9d9; 
		height: 250px; 
		overflow-y: auto; 
		padding-left: 10px;
		font-size: 16px;"
	}
	.ketlan{
		border: 2px solid #d9d9d9;
	}
	@media (max-width: 768px) {
		.a{
			display: block;
			margin-left: auto;
			margin-right: auto;
		}
	</style>
	<nav aria-label="breadcrumb" class="mt-3">
		<h6>Barang</h6>
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#"><i class="fas fa-cube"></i> Barang</a></li>
			<li class="breadcrumb-item active"><a href="#"></i> data barang</a></li>
		</ol>
	</nav>

	<div class="card mt-3">
		<div class="card-header">
			<div class="row">
				<div class="col">
					List Barang
				</div>
				<div class="col text-right">
					<button style="font-size: 15px;" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#tambah"><i class="fas fa-plus"></i> Tambah</button>
				</div>
			</div>
		</div>
		<div class="card-body card-font">
			<!-- content -->
			<table id="example" class="table table-striped table-bordered table-hover" style="width: 100%;">
				<thead>
					<tr style="text-align: center;">
						<th style="width: 150px;">Nama Barang</th>
						<th style="width: 100px;">Harga Awal</th>
						<th style="width: 210px;">Dilelang Pada Tanggal</th>
						<th style="width: 120px;">Status</th>
						<th style="width: 130px;">Aksi</th>
					</tr>
				</thead>
				<tbody>
					@foreach($barang as $b)
					<tr class="item{{$b->id_barang}}">
						<td>{{$b->nama_barang}}</td>
						<td>Rp. {{number_format($b->harga_awal, 0, ".", ".")}}</td>
						<td class="text-center">
							@if($b->tgl_mulai_lelang == null)
							-
							@else
							{{ Carbon\Carbon::parse($b->tgl_mulai_lelang)->formatLocalized('%d %B %Y. Jam %H:%M')}}
							@endif
						</td>
						<td style="text-align: center;">
							@if($b->status == 'Belum Diatur')
							<button type="button" style="cursor: default;" class="btn btn-secondary btn-sm lev">{{$b->status}}</button>
							@elseif($b->status == 'Akan Datang')
							<button type="button" style="cursor: default;" class="btn btn-success btn-sm lev">{{$b->status}}</button>
							@elseif($b->status == 'Dibuka')
							<button type="button" style="cursor: default;" class="btn btn-primary btn-sm lev">{{$b->status}}</button>
							@elseif($b->status == 'Ditutup')
							<button type="button" style="cursor: default;" class="btn btn-danger btn-sm lev">{{$b->status}}</button>
							@endif
						</td>
						<td style="text-align: center;">
							@if($b->status == 'Belum Diatur')
							<button class="btn btn-outline-primary btn-sm btn-ling lelang-modal" data-id_barang="{{$b->id_barang}}" data-id_lelang="{{$b->id_lelang}}" data-nama="{{$b->nama_barang}}">
								<i class="fas fa-play"></i>
							</button>
							@elseif($b->status == 'Akan Datang')
							<button class="btn btn-success btn-sm btn-ling" style="cursor: default;">
								<i class="fas fa-pause"></i>
							</button>
							@elseif($b->status == 'Dibuka')
							<button class="btn btn-warning btn-sm btn-ling stop-modal" data-nama="{{$b->nama_barang}}" data-id_lelang="{{$b->id_lelang}}" data-id_barang="{{$b->id_barang}}">
								<i class="fas fa-stop"></i>
							</button>
							@elseif($b->status == 'Ditutup')
							<button class="btn btn-outline-danger btn-sm btn-ling" style='cursor: default;'>
								<i class="fas fa-lock"></i>
							</button>
							@endif
							<button class="btn btn-outline-dark btn-sm btn-ling info-modal" data-id="{{$b->id_barang}}" data-nama="{{$b->nama_barang}}" data-harga_awal="{{number_format($b->harga_awal, 0, '.', '.')}}" data-harga_akhir="{{number_format($b->harga_akhir, 0, '.', '.')}}" data-deskripsi="{{$b->deskripsi_barang}}" data-gambar="{{$b->foto_barang}}" data-status="{{$b->status}}" data-tgl_mulai="{{$b->tgl_mulai_lelang}}" data-tgl_selesai="{{$b->tgl_selesai_lelang}}">
								<i class="fas fa-info"></i>
							</button>
							@if($b->status == 'Belum Diatur')
							<button class="btn btn-outline-primary btn-sm btn-ling edit-modal" data-id="{{$b->id_barang}}" data-nama="{{$b->nama_barang}}" data-harga="{{number_format($b->harga_awal, 0, '.', '.')}}" data-deskripsi="{{$b->deskripsi_barang}}" data-gambar="{{$b->foto_barang}}">
								<i class="fas fa-user-edit"></i>
							</button>
							@endif
							<button class="btn btn-outline-danger btn-sm btn-ling delete-modal" data-id="{{$b->id_barang}}" data-name="{{$b->nama_barang}}">
								<i class="fas fa-trash-alt"></i>
							</button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<!-- stop lelang-->
	<div class="modal fade" id="stopModal" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title">
						Tutup Lelang
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form class="stop_form">
					<div class="modal-body">
						Anda Yakin Ingin Menutup Lelang ?? <span class="did_lel d-none"></span>
						<span class="did_bar d-none"></span>
						<br>Nama Barang : <span class="dnama"></span>
						<br><span style="font-size: 10px; color: red;">Lelang Yang Sudah Ditutup Tidak Bisa Dibuka Kembali</span>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-danger btn-sm kunci" data-dismiss="modal"><i class="fas fa-lock"></i> Tutup</button>
						<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- end stop -->

	<!-- aksi lelang -->
	<div class="modal fade" id="aksi_lelang" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title" id="exampleModalLabel">
						Lelang
					</div>
					<button type="button" class="close tutup" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form id="form_lelang" enctype="multipart/form-data">
					@csrf
					<div class="modal-body">
						<input type="hidden" name="id_barang" id="id_bar">
						<input type="hidden" name="id_lelang" id="id_lel">
						<input type="hidden" name="id_petugas" value="{{Auth::guard('admin')->user()->id_petugas}}">
						<div class="form-group">
							<label for="inputAddress">Nama Barang</label>
							<input type="text" class="form-control form-control-sm bar" name="barang" readonly>
						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Lelang Pada Tanggal</label>
								<input type="text" class="form-control form-control-sm datepicker" name="tgl_awal" readonly>
							</div>
							<div class="form-group col-md-6">
								<label>Dimulai Jam</label>
								<input type="text" class="form-control form-control-sm jam" name="jam_awal" readonly>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label>Berakhir Saat Tanggal</label>
								<input type="text" class="form-control form-control-sm datepicker" name="tgl_akhir" readonly>
							</div>
							<div class="form-group col-md-6">
								<label>Pada Jam</label>
								<input type="text" class="form-control form-control-sm jam" name="jam_akhir" readonly>
							</div>
						</div>

					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary btn-sm save_lelang">Mulai Lelang</button>
						<button type="button" class="btn btn-secondary btn-sm tutup" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- end aksi lelang -->

	<!-- info modal -->
	<div class="modal fade bd-example-modal-lg" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h6 class="modal-title" id="exampleModalLabel">Informasi Barang</h6>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-5 mt-2">
							<a class="fancy" data-fancybox>
								<div class="a">
									<img class="f" style="height:450px; object-fit: cover;">
								</div>
							</a>
						</div>
						<div class="col-md-7 mt-2 ml-auto">
							<div class="desk">
								<label>Deskripsi Barang : </label>
								<p class="des">
								</p>
							</div>
						</div>
					</div>
					<div class="row mt-2">
						<div class="col">
							<div class="ketlan" style="padding: 10px;">
								<table style="font-size: 16px;">
									<tr>
										<td class="names">Nama Barang</td>
										<td style="width: 30px; text-align: center;">:</td>
										<td class="jeneng"></td>
									</tr>
									<tr>
										<td class="names">Harga Awal</td>
										<td style="width: 30px; text-align: center;">:</td>
										<td class="rego_awal"></td>
									</tr>
									<tr>
										<td class="names">Harga AKhir</td>
										<td style="width: 30px; text-align: center;">:</td>
										<td class="rego_akhir"></td>
									</tr>
									<tr>
										<td class="names">Durasi Lelang</td>
										<td style="width: 30px; text-align: center;">:</td>
										<td class="durasi"></td>
									</tr>
									<tr>
										<td class="names">Status</td>
										<td style="width: 30px; text-align: center;">:</td>
										<td class="sta"></td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!-- end info modal -->

	<!-- Modal create data -->
	<div class="modal fade" id="tambah" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title" id="exampleModalLabel">
						Tambah Barang
					</div>
					<button type="button" class="close tutup" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form id="sample_form" enctype="multipart/form-data">
					@csrf
					<div class="modal-body">
						<div class="form-group">
							<label for="inputAddress">Nama Barang</label>
							<input type="text" class="form-control form-control-sm" name="barang" required>
						</div>

						<div class="form-group">
							<label>Mulai Lelang Dengan Harga</label>
							<div class="input-group input-group-sm mb-2">
								<div class="input-group-prepend">
									<div class="input-group-text">Rp.</div>
								</div>
								<input data-mask="000.000.000" data-mask-reverse="true" type="text" class="form-control form-control-sm" name="harga">
							</div>
						</div>

						<div class="form-group">
							<label>Deskripsi Barang</label>
							<textarea class="form-control form-control-sm" name="deskripsi" rows="3"></textarea>
						</div>

						<div class="form-group">
							<label>Foto Barang</label>
							<input type="file" id="file" name="fotos" onchange="return fileValidation()"/>
							<!-- Image preview -->
							<div id="imagePreview" class="mt-2"></div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" id="m" class="btn btn-primary btn-sm">Tambah</button>
						<button type="button" class="btn btn-secondary btn-sm tutup" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- end modal create -->

	<!-- Modal edit data -->
	<div class="modal fade" id="edit" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<form id="form_edit">
					<div class="modal-header">
						<div class="modal-title" id="exampleModalLabel">
							Edit Barang
						</div>
						<button type="button" class="close tutup" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body pb-0">
						<input type="hidden" name="id_barang" class="id" required>
						<div class="form-group">
							<label>Nama Barang</label>
							<input type="text" class="nama form-control form-control-sm" name="barang" required>
						</div>

						<div class="form-group">
							<label>Mulai Lelang Dengan Harga</label>
							<div class="input-group input-group-sm mb-2">
								<div class="input-group-prepend">
									<div class="input-group-text">Rp.</div>
								</div>
								<input data-mask="000.000.000" data-mask-reverse="true" type="text" class="harga form-control form-control-sm" name="harga" required>
							</div>
						</div>

						<div class="form-group">
							<label>Deskripsi Barang</label>
							<textarea class="deskripsi form-control form-control-sm" name="deskripsi" rows="3"></textarea>
						</div>

						<div class="form-group">
							<label>Foto Barang</label>
							<input type="file" id="files" name="fotos" onchange="return fileValidations()"/>
							<!-- Image preview -->
							<div id="imagePreviews" class="mt-3">
								<a id="gamb" data-fancybox><img id="gim" style="max-height: 147px;"/></a>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary btn-sm edit">Update</button>
						<button type="button" class="btn btn-secondary btn-sm tutup" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- end modal edit -->

	<!-- modal delete -->
	<div class="modal fade" id="myModal" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title" id="exampleModalLabel">
						Hapus Data
					</div>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form class="sample_form">
					<div class="modal-body">
						Anda Yakin Untuk Menghapus Data <span class="dname"></span> ?? <span style="display: none;" class="hidden did"></span>
					</div>
					<div class="modal-footer">
						<button type="submit" class="btn btn-danger btn-sm delete" data-dismiss="modal"><i class="fas fa-trash-alt"></i> Delete</button>
						<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- end modal delete -->

	@endsection
	@section('crud')
	<script type="text/javascript" src="{{url('js/admin/crud_barang.js')}}"></script>
	<script type="text/javascript">
		$(function(){
			$(".datepicker").datepicker({
				format: 'dd-mm-yyyy',
				autoclose: true,
			});
			$(".jam").clockpicker({
				placement: 'top',
				default : 'now',
				autoclose:true
			});
		});
	</script>
	@endsection