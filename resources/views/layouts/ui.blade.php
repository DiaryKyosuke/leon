
<!DOCTYPE html>
<html>
<head>
	<!-- require meta tags -->
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1, shink-to-fit-no">
	<title>LEON</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
	<!-- bootstrap css -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<!-- fancybox -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
	<!-- my css -->
	<link rel="stylesheet" type="text/css" href="{{url('css/ui/ui.css')}}">
	<link rel="shortcut icon" href="{{url('foto/logo.ico')}}" />
	
</head>
<body>

	<!-- preloader -->
	<div id="preloader">
		<div class="loder-box">
			<div class="battery"></div>
		</div>
	</div>
	<!-- end preloader -->
	<!-- navbar -->

	<nav id="navbar" class="navbar navbar-expand-md navbar-dark">
		<div class="container">
			<a class="navbar-brand" href="">LEON</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="menu">
				<div class="navbar-nav ml-auto">
					<a class="nav-item nav-link @yield('beranda','')" href="/">Beranda<span class="sr-only">(current)</span></a>
					<a class="nav-item nav-link @yield('coming','')" href="akan">Coming Soon<span class="sr-only">(current)</span></a>
					<a class="nav-item nav-link" href="sign">SIGN IN</a>
				</div>
			</div>
		</div>
	</nav>
	<!-- end navbar -->

	<div class="contents">
		<!-- jumbotron -->
		<div class="jumbotron jumbotron-fluid">
			<div class="container">
				<h1 class="wow animated fadeInDown">Bingung mau cari lelang online dimana?<br> Coba <span>LEON</span> sekarang juga!</h1>
				<a href="sign" class="btn btn-primary tombol">Gabung Sekarang!</a>
			</div>
		</div>
		<!-- end jumbotron -->
		@yield('content')
		<!-- Site footer -->
		<footer class="site-footer">
			<div class="ml-4">
				<div class="row">
					<div class="col-md-8 col-sm-6 col-xs-12">
						<p class="copyright-text">Copyright &copy; 2020 Design & Build by 
							<a href="https://www.instagram.com/fendi.anwar.rifai/">Fendi Anwar Rifa'i</a>.
						</p>
					</div>
				</div>
			</div>
		</footer>
		<!-- end footer -->
	</div>

	<!--jquery, pooper.js, bootstrap.js-->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>
	<script type="text/javascript" src="{{'js/ui/wow.js'}}"></script>
	<script type="text/javascript" src="{{'js/ui/ui.js'}}"></script>
	@yield('waktu')
	</body>
	</html>