<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('judul','Lelang Online')</title>

	<!-- Bootstrap CSS CDN -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
	<!-- Font Awesome JS -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
	
	<!-- animate -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

	<!-- datatables -->
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">

	<!-- fancybox -->
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
	<!-- css gawean dewe -->
	<link rel="stylesheet" type="text/css" href="{{url('css/user/costom.css')}}">
	<link rel="shortcut icon" href="{{url('foto/logo.ico')}}" />
</head>

<body>
	<!-- preloader -->
	<div id="preloader">
		<div class="loder-box">
			<div class="battery"></div>
		</div>
	</div>
	<!-- end preloader -->

	<div class="wrapper">
		<!-- Iki Sidebar responsive -->
		<nav id="sidebar">
			<div id="x">
				<i class="fas fa-times"></i>
			</div>
			<div class="sidebar-header">
				<center>
					<img src="">
					<span>{{Auth::guard('user')->user()->nama}}</span>
				</center>
			</div>

			<ul class="list-unstyled components">
				<p>Apl Lelang Online</p>
				<li class="@yield('ber','')">
					<a href="beranda">
						<i class="fas fa-home"></i> Beranda
					</a>
				</li>
				<li class="@yield('lels','')">
					<a href="#lelangs" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-boxes"></i> Pelelangan</a>
					<ul class="collapse list-unstyled" id="lelangs">
						<li>
							<a href="coming"><i class="fas fa-truck-loading"></i> Akan Datang</a>
						</li>
						<li>
							<a href="running"><i class="fas fa-truck-moving"></i> Sedang Berjalan</a>
						</li>
						<li>
							<a href="history"><i class="fas fa-history"></i> History Lelang</a>
						</li>
					</ul>
				</li>
				<li class="@yield('akuns','')">
					<a href="profil">
						<i class="fas fa-cog"></i> Pengaturan Akun
					</a>
				</li>
			</ul>

			<ul class="list-unstyled CTAs">
				<li>
					<a href="logout" class="keluar">Logout</a>
				</li>
			</ul>
		</nav>
		<!-- end sidebar -->
		<div id="content">
			<nav class="navbar navbar-expand-lg navbar-dark" id="navbar">
				<div class="container-fluid">
					<!-- navbar ketika responsive -->
					<button type="button" id="sidebarCollapse" class="togglemenu btn">
						<i class="fas fa-align-left"></i>
					</button>
					<!-- button logout -->
					<a href="logout" class="btn d-inline-block d-lg-none ml-auto cus" type="button">
						<i class="fas fa-sign-out-alt"></i>
					</a>

					<!-- navbar dekstop -->
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav text-white nav-kir">
							<li class="nav-item @yield('beranda','')">
								<a class="mr-2" href="beranda">
									<i class="fas fa-home"></i> Beranda
								</a>
							</li>
							<li class="nav-item dropdown @yield('pelelangan','')">
								<a href="#" class="dropdown-toggle"  data-toggle="dropdown">Pelelangan</a>
								<div class="dropdown-menu custam">
									<a class="dropdown-item @yield('akan','')" href="coming">Akan Datang</a>
									<a class="dropdown-item @yield('sedang','')" href="running">Sedang Berjalan</a>
									<a class="dropdown-item @yield('history','')" href="history">History Lelang</a>
								</div>
							</li>
						</ul>
					</div>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="nav navbar-nav ml-auto text-white">
							<li class="nav-item dropdown mr-2">
								<a href="#" class="dropdown-toggle"  data-toggle="dropdown">Hello, Pelelang </a>
								<div class="shadow dropdown-menu dropdown-menu-right dropdown-cus">
									<div class="card-body text-center p-0">
										<div class="img-bunder">
											<img src="{{url('foto/user6.png')}}" class="rounded-circle border border-light" height="100px" alt="">
										</div>
										<div class="mb-2 inf">
											<span class="font-weight-bold">{{Auth::guard('user')->user()->nama}}</span>
											<br>
											<span class="text-muted info-cus">{{Auth::guard('user')->user()->email}}</span>
											<br>
											<a href="profil" class="mt-3 tombol-set">Kelola Akun Anda</a>
										</div>
										<hr class="m-0">
										<div class="mb-1">
											<a href="logout" class="tombol-metu">Keluar</a>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>
			@yield('content')
			<!-- Site footer -->

			<!-- end footer -->
		</div>
		<div class="overlay"></div>
	</div>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
	<script type="text/javascript" src="{{url('js/ui/wow.js')}}"></script>
	<script type="text/javascript" src="{{url('js/user/custom.js')}}"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
	@include('sweet::alert')
	@yield('waktu')
</body>
</html>
