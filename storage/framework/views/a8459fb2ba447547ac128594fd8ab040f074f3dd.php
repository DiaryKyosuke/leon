<?php $__env->startSection('judul','Pengaturan Akun'); ?>
<?php $__env->startSection('profile','active'); ?>
<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb" class="mt-3">
	<h6>Profile</h6>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="#"><i class="fas fa-user-alt"></i> Profile</a></li>
		<li class="breadcrumb-item active"><a href="#"></i>pengaturan</a></li>
	</ol>
</nav>

<div class="row mt-3">
	
	<div class="col-md-5">
		<div class="card shadow edit">
			<div class="card-body">
				<h5 class="card-title">Pengaturan Akun</h5>
				<form id="akun" method="post" action="ubah_akun">
					<?php echo csrf_field(); ?>
					<div class="form-group">
						<input class="id_petugas" type="hidden" name="id_petugas" value="<?php echo e($data->id_petugas); ?>" required>

						<label>Nama Lengkap</label>
						<input type="text" class="form-control form-control-sm" name="nama" value="<?php echo e($data->nama); ?>" required>
					</div>
					<div class="form-group ">
						<label>Email Aktif</label>
						<input type="text" class="form-control form-control-sm" name="email" value="<?php echo e($data->email); ?>" required>
					</div>
					<hr>
					<div class="form-group row float-right">
						<div class="col">
							<button type="submit" class="btn btn-primary btn-sm shadow">Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="col-md-7">
		<div class="card shadow edit">
			<div class="card-body">
				<h5 class="card-title">Ganti Password</h5>
				<form id="ganti_pw" method="post" action="ubah_pw">
					<?php echo csrf_field(); ?>
					<div class="form-group">
						<input type="hidden" name="id_petugas" value="<?php echo e($data->id_petugas); ?>" required>

						<label>Password Lama</label>
						<input type="password" id="lama" class="pw form-control form-control-sm" name="pw_lama" required>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label>Password Baru</label>
							<input type="password" id="baru" class="pw form-control form-control-sm" name="pw_baru" required>
						</div>
						<div class="form-group col-md-6">
							<label>Konfirmasi Password</label>
							<input type="password" id="Konfirmasi" class="pw form-control form-control-sm" name="konfirmasi_pw" required>
						</div>
					</div>
					<div class="form-group">
						<div class="custom-control custom-checkbox mb-3">
							<input type="checkbox" class="custom-control-input show" id="customCheck1">
							<label class="custom-control-label" for="customCheck1" style="top: 25rem;">Lihat password</label>
						</div>
					</div>
					<hr>
					<div class="form-group row float-right">
						<div class="col">
							<button id="submit" type="submit" class="btn btn-primary btn-sm shadow">Simpan Perubahan</button>
							<button type="reset" class="btn btn-danger btn-sm shadow">Reset</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('crud'); ?>
<script type="text/javascript">
	$('.show').click(function(){
		if($(this).is(':checked')){
			$('.pw').attr('type','text');
		}else{
			$('.pw').attr('type','password');
		}
	});

	$("#submit").click(function () {
		var password = $("#baru").val();
		var confirmPassword = $("#Konfirmasi").val();
		if (password != confirmPassword) {
			swal({
				icon: 'error',
				title: 'Oops...',
				text: 'Sandi Konfirmasi Tidak Cocok!'
			});
			return false;
		}
		return true;
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/lelang_online/resources/views/admin/profile.blade.php ENDPATH**/ ?>