<?php $__env->startSection('judul','History Lelang'); ?>
<?php $__env->startSection('barang','active'); ?>
<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb" class="mt-3">
	<h6>Barang</h6>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="#"><i class="fas fa-cube"></i> Barang</a></li>
		<li class="breadcrumb-item active"><a href="#"></i> history</a></li>
	</ol>
</nav>

<div class="card mt-3">
	<div class="card-header">
		<div class="row">
			<div class="col">
				History Lelang
			</div>
		</div>
	</div>
	<div class="card-body card-font">
		<!-- content -->
		<table id="example" class="table table-striped table-bordered table-hover" style="width: 100%;">
			<thead>
				<tr style="text-align: center;">
					<th style="width: 5px;">No.</th>
					<th style="width: 150px;">Nama Barang</th>
					<th style="width: 100px;">Foto</th>
					<th style="width: 160px;">Pemenang</th>
					<th style="width: 80px;">Hrg. Awal</th>
					<th style="width: 80px;">Hrg. Akhir</th>
				</tr>
			</thead>
			<tbody>
				<?php $__currentLoopData = $barang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $b): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td style="text-align: center;"><?php echo e(++$i); ?></td>
					<td><?php echo e($b->nama_barang); ?></td>
					<td style="text-align: center;">
						<div class="crop">
							<a data-fancybox href="/foto/barang/<?php echo e($b->foto_barang); ?>">
								<button class="btn btn-outline-primary btn-sm btn-ling">
								<i class="fas fa-image"></i>
							</button>
							</a>
						</div>
					</td>
					<td style="text-align: center;"> 
						<?php if($b->id_user == null): ?>
							<button type="button" style="cursor: default;" class="btn btn-danger btn-sm lev">Tidak Ada Pemenang</button>
						<?php else: ?>
							<?php $data = $users->where('id','=',$b->id_user)->first(); ?>
							<button type="button" style="cursor: default;" class="btn btn-primary btn-sm lev"><?php echo e($data->nama); ?></button>
						<?php endif; ?>
					</td>
					<td style="text-align: center;">Rp. <?php echo e(number_format($b->harga_awal, 0, ".", ".")); ?></td>
					<td style="text-align: center;">Rp. <?php echo e(number_format($b->harga_akhir, 0, ".", ".")); ?></td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</tbody>
		</table>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/lelang_online/resources/views/admin/history.blade.php ENDPATH**/ ?>