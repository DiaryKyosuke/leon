<?php $__env->startSection('judul','Detail Barang'); ?>
<?php $__env->startSection('content'); ?>
 <style type="text/css">
 	.tom .card-header:after {
 		font-family: 'FontAwesome';  
 		content: "\f077";
 		float: right; 
 	}
 	.tom .card-header.collapsed:after {
 		/* symbol for "collapsed" panels */
 		content: "\f078"; 
 	}
 	.a{
 		border: 2px solid #d9d9d9;
 		overflow: hidden; 
 		width: 260px; 
 		height: 250px;
 	}
 	.nohover:hover {
 		/* here copy default .btn class styles */
 		cursor:default !important;
 		color: #28a745;
 		background-color: transparent;
 		background-image: none;
 		border-color: #28a745;
 		/* or something like that */
 	}
 	@media (max-width: 768px) {
 		.a{
 			display: block;
 			margin-left: auto;
 			margin-right: auto;
 			width: 240px; 
 			height: 240px;
 		}
 		.hc{
 			font-size: 20px;
 		}
 		.font , .btn-med , .wektu{
 			font-size: 15px;
 		}
 		.post{
 			font-size: 18px;
 		}
 		.tawaran{
 			font-size: 16px;
 		}
 	}
 </style>
 <div class="container" id="reload">		
 	<div class="row">
 		<div class="col-md-3 mt-3">
 			<div id="accordion" class="tom">
 				<div class="card mb-0">
 					<div class="card-header" data-toggle="collapse" href="#collapseOne">
 						<a class="card-title" style="font-size: 12px;"><b>ESTIMASI WAKTU SERVER</b></a>
 					</div>
 					<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
 						<div class="card-body text-center">
 							<!-- <b>19 Feb 2020, 19:30</b> -->
 							<b class="wektu"><span id="date"></span>, <span id="jam"></span>:<span id="menit"></span>:<span id="detik"></span></b>
 						</div>
 					</div>
 				</div>
 			</div>
 			<div class="mt-3">
 				<a class="fancy" data-fancybox href="/foto/barang/<?php echo e($barang->foto_barang); ?>">
 					<div class="a">
 						<img class="f" style="height:260px; object-fit: cover;" src="/foto/barang/<?php echo e($barang->foto_barang); ?>">
 					</div>
 				</a>
 			</div>
 		</div>

 		<div class="col mt-3 font">
 			<h4 class="hc"><?php echo e($barang->nama_barang); ?></h4>
 			<hr>
 			<div>
 				<p class="availability" style="margin: 0px;">
 					<span style="font-size: 20px;">Harga Awal:</span>
 				</p>
 				<div>
 					<h4 class="hc">Rp. <?php echo e(number_format($barang->harga_awal, 0, ".", ".")); ?></h4>
 				</div>
 			</div>

 			<table class="table table-striped">
 				<tbody>
 					<tr>
 						<td width="40%">
 							Deskripsi Barang
 						</td>
 						<td>
 							<?php echo $barang->deskripsi_barang; ?>
 						</td>
 					</tr>
 					<tr>
 						<td>
 							Tanggal Mulai Lelang
 						</td>
 						<td class="mulai">

 						</td>
 					</tr>
 					<tr>
 						<td>
 							Batas Akhir Penawaran
 						</td>
 						<td class="selesai">
 						</td>
 					</tr>
 				</tbody>
 			</table>

 			<div class="col-md-12 col-xs-12">
 				<a href="sign" class="btn btn-primary btn-med btn-block ikuti">
 					<i class="fa fa-check-square"></i>
 					<span>Silahkan Login Untuk Mengikut Lelang</span>
 				</a>
 			</div>
 		</div>
 	</div>
 </div>
 <input type="hidden" name="" id="tgl_awal" value="<?php echo e($barang->tgl_mulai_lelang); ?>">
 <input type="hidden" name="" id="tgl_selesai" value="<?php echo e($barang->tgl_selesai_lelang); ?>">
 <?php $__env->stopSection(); ?>
 <?php $__env->startSection('waktu'); ?>
 <script type="text/javascript">
 	window.setTimeout("waktu()", 100);

 	function waktu() {
 		var waktu = new Date();
 		setTimeout("waktu()", 100);
 		jam = waktu.getHours();
 		menit = waktu.getMinutes();
 		detik = waktu.getSeconds();
 		document.getElementById("jam").innerHTML = ("0" + jam).slice(-2);
 		document.getElementById("menit").innerHTML = ("0" + menit).slice(-2);
 		document.getElementById("detik").innerHTML =  ("0" + detik).slice(-2);
 	}
 	$(document).ready(function() {
 		$.ajaxSetup({
 			headers: {
 				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
 			}
 		});
 		var months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agust', 'Sep', 'Okt', 'Nov', 'Des'];
 		var date = new Date();
 		var day = date.getDate();
 		var month = date.getMonth();
 		var yy = date.getYear();
 		var year = (yy < 1000) ? yy + 1900 : yy;
 		$dates = (day + ' ' + months[month] + ' ' + year);
 		document.getElementById("date").innerHTML = $dates;

 		var namabulan = ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'];
 		var da = $('#tgl_awal').val();
 		var tgl = new Date(da);
 		var tanggal = tgl.getDate();
 		var bulan = tgl.getMonth();
 		var tahun = tgl.getFullYear();
 		$tgl_mulai_lelang = tanggal + " " + namabulan[bulan] + " " + tahun;
 		$('.mulai').text($tgl_mulai_lelang+", Jam "+tgl.getHours()+":"+tgl.getMinutes());

 		var dates = $('#tgl_selesai').val();
 		var bar = new Date(dates);
 		var btanggal = bar.getDate();
 		var bbulan = bar.getMonth();
 		var btahun = bar.getFullYear();
 		$tgl_selesai_lelang = btanggal + " " + namabulan[bbulan] + " " + btahun;
 		$('.selesai').text($tgl_selesai_lelang+", Jam "+bar.getHours()+":"+bar.getMinutes());
 	});
 </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.ui', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/lelang_online/resources/views/detail_barang.blade.php ENDPATH**/ ?>