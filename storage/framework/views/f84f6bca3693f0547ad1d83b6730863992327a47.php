<?php $__env->startSection('judul','Dashboard'); ?>
<?php $__env->startSection('dashboard','active'); ?>
<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb" class="mt-3">
    <h6>Dashboard</h6>
    <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="#"><i class="fas fa-home"></i> Home</a></li>
    </ol>
</nav>

<div class="row">
    <div class="col-md-3 mt-3">
        <div class="card border-primary mx-sm-1 p-3">
            <div class="card border-primary shadow text-primary p-3 my-card"><span class="fas fa-cube" aria-hidden="true"></span></div>
            <div class="text-primary text-center mt-3"><h6>Barang</h6></div>
            <div class="text-primary text-center mt-2"><h5><?php echo e($barang); ?></h5></div>
        </div>
    </div>
    <div class="col-md-3 mt-3">
        <div class="card border-dark mx-sm-1 p-3">
            <div class="card border-dark shadow text-dark p-3 my-card" ><span class="fas fa-truck-loading" aria-hidden="true"></span></div>
            <div class="text-dark text-center mt-3"><h6>Akan Datang</h6></div>
            <div class="text-dark text-center mt-2"><h5><?php echo e($datang); ?></h5></div>
        </div>
    </div>
    <div class="col-md-3 mt-3">
        <div class="card border-success mx-sm-1 p-3">
            <div class="card border-success shadow text-success p-3 my-card" ><span class="fas fa-box-open" aria-hidden="true"></span></div>
            <div class="text-success text-center mt-3"><h6>Lelang Dibuka</h6></div>
            <div class="text-success text-center mt-2"><h5><?php echo e($buka); ?></h5></div>
        </div>
    </div>
    <div class="col-md-3 mt-3">
        <div class="card border-danger mx-sm-1 p-3">
            <div class="card border-danger shadow text-danger p-3 my-card" ><span class="fas fa-box" aria-hidden="true"></span></div>
            <div class="text-danger text-center mt-3"><h6>Lelang Ditutup</h6></div>
            <div class="text-danger text-center mt-2"><h5><?php echo e($tutup); ?></h5></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="card mt-3" style="width: 100%;">
    <div class="card-header">
        <div class="row">
            <div class="col">
                History Lelang
            </div>
        </div>
    </div>
    <div class="card-body card-font">
        <!-- content -->
        <table id="example" class="table table-striped table-bordered table-hover" style="width: 100%;">
            <thead>
                <tr style="text-align: center;">
                    <th style="width: 5px;">No.</th>
                    <th style="width: 130px;">Nama Barang</th>
                    <th style="width: 100px;">Foto</th>
                    <th style="width: 160px;">Pemenang</th>
                    <th style="width: 80px;">Hrg. Awal</th>
                    <th style="width: 80px;">Hrg. Akhir</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $barangs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $b): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td style="text-align: center;"><?php echo e(++$i); ?></td>
                    <td><?php echo e($b->nama_barang); ?></td>
                    <td style="text-align: center;">
                        <div class="crop">
                            <a data-fancybox href="/foto/barang/<?php echo e($b->foto_barang); ?>">
                                <button class="btn btn-outline-primary btn-sm btn-ling">
                                <i class="fas fa-image"></i>
                            </button>
                            </a>
                        </div>
                    </td>
                    <td style="text-align: center;"> 
                        <?php if($b->id_user == null): ?>
                            <button type="button" style="cursor: default;" class="btn btn-danger btn-sm lev">Tidak Ada Pemenang</button>
                        <?php else: ?>
                            <?php $data = $users->where('id','=',$b->id_user)->first(); ?>
                            <button type="button" style="cursor: default;" class="btn btn-primary btn-sm lev"><?php echo e($data->nama); ?></button>
                        <?php endif; ?>
                    </td>
                    <td style="text-align: center;">Rp. <?php echo e(number_format($b->harga_awal, 0, ".", ".")); ?></td>
                    <td style="text-align: center;">Rp. <?php echo e(number_format($b->harga_akhir, 0, ".", ".")); ?></td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
</div>

</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.admin.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/lelang_online/resources/views/admin/dashboard.blade.php ENDPATH**/ ?>