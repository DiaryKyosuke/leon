<?php $__env->startSection('judul','Detail Lelang Anda'); ?>
<?php $__env->startSection('content'); ?>
<div class="container" style="padding-bottom: 100px;">		
	<div class="row">
		<div class="col-md-3 mt-3">
			<div id="accordion" class="tom">
				<div class="card mb-0">
					<div class="card-header" data-toggle="collapse" href="#collapseOne">
						<a class="card-title" style="font-size: 12px;"><b>ESTIMASI WAKTU SERVER</b></a>
					</div>
					<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body text-center">
							<!-- <b>19 Feb 2020, 19:30</b> -->
							<b class="wektu"><span id="date"></span>, <span id="jam"></span>:<span id="menit"></span>:<span id="detik"></span></b>
						</div>
					</div>
				</div>
			</div>
			<div class="mt-3">
				<a class="crop" data-fancybox href="/foto/barang/<?php echo e($barang->foto_barang); ?>">
					<img class="card-img-top img-fluid" src="/foto/barang/<?php echo e($barang->foto_barang); ?>" alt="Card image cap">
				</a>
			</div>
		</div>

		<div class="col mt-3">
			<h4 class="hc"><?php echo e($barang->nama_barang); ?></h4>
			<hr>
			<div>
				<p class="availability" style="margin: 0px;">
					<span style="font-size: 20px;">Harga Awal:</span>
				</p>
				<div>
					<h4 class="hc">Rp. <?php echo e(number_format($barang->harga_awal, 0, ".", ".")); ?></h4>
				</div>
			</div>

			<table class="table table-striped">
				<tbody>
					<tr>
						<td colspan="2">
							<?php echo $barang->deskripsi_barang; ?>
						</td>
					</tr>
					<tr>
						<td>
							Tanggal Mulai Lelang
						</td>
						<td class="mulai">

						</td>
					</tr>
					<tr>
						<td>
							Batas Akhir Penawaran
						</td>
						<td class="selesai">
						</td>
					</tr>
				</tbody>
			</table>

			<div class="col-md-12 col-xs-12" id="iki_button">
				<?php if($barang->status == 'Dibuka'): ?>
				<button class="testing nohover btn btn-outline-success btn-med btn-block">
					<i class="fa fa-check-square"></i>
					<span> Di Ikuti</span>
				</button>
				<?php elseif($barang->status == 'Ditutup'): ?>
				<button style="cursor:default !important;" class="btn btn-danger btn-med btn-block">
					<i class="fas fa-times-circle"></i>
					<span> Lelang Yang Anda Ikuti Sudah Tutup</span>
				</button>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="row mt-4" id="taw">
		<div class="col-md-4 mt-3">
			<?php if($barang->status !== 'Ditutup'): ?>
			<form id="taw_barang">
				<?php echo csrf_field(); ?>
				<div class="form-row align-items-center">
					<div class="col">
						<div class="input-group input-group-sm">
							<div class="input-group-prepend">
								<div class="input-group-text">Rp. </div>
							</div>
							<input type="hidden" name="id_lelang" value="<?php echo e($barang->id_lelang); ?>">
							<input type="hidden" name="id_barang" value="<?php echo e($barang->id_barang); ?>">
							<input type="hidden" name="id_user" value="<?php echo e($users->id); ?>">
							<input name="tawars" data-mask="000.000.000" data-mask-reverse="true" type="text"class="form-control form-control-sm" placeholder="Tawaran Anda">
						</div>
					</div>
					<div class="col-auto my-1">
						<button type="submit" class="btn btn-primary btn-sm" id="tawr">Tawar</button>
					</div>
				</div>
			</form>
			<?php endif; ?>
		</div>
		<div class="col mt-3 coment">
			<div id="refresh">
				<?php $__currentLoopData = $tawar; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $t): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<div class="media border p-3 mb-2 mt-3">
					<img src="<?php echo e(url('foto/user6.png')); ?>" alt="foto-user" class="mr-3 mt-1 rounded-circle" style="width:60px;">
					<div class="media-body">
						<div class="row">
							<div class="col">
								<font class="post" size="4px;"><b><?php echo e($t->nama); ?></b> <small> Posted on <i><?php echo e($t->created_at); ?></i></small></font>
								<br>
								<font class="tawaran" size="3px;">Rp. <?php echo e(number_format($t->penawaran_harga, 0, ".", ".")); ?></font>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div>
		</div>
	</div>

</div>
<input type="hidden" name="" id="tgl_awal" value="<?php echo e($barang->tgl_mulai_lelang); ?>">
<input type="hidden" name="" id="tgl_selesai" value="<?php echo e($barang->tgl_selesai_lelang); ?>">
<input type="hidden" name="id_lelang" id="id_lel" value="<?php echo e($barang->id_lelang); ?>">
<input type="hidden" name="id_barang" id="id_barang" value="<?php echo e($barang->id_barang); ?>">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('waktu'); ?>
<script type="text/javascript" src="<?php echo e(url('js/user/rincian_history.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/lelang_online/resources/views/user/history_rincian.blade.php ENDPATH**/ ?>