<?php $__env->startSection('judul','Login'); ?>
<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
		<div class="card card-signin my-5">
			<div class="card-body">
				<h5 class="card-title text-center">Sign In</h5>
				<form class="form-signin" method="post" action="masuk">
					<?php echo csrf_field(); ?>
					<div class="form-label-group">
						<input type="text" class="form-control" placeholder="Username" name="username" required>
					</div>

					<div class="form-label-group">
						<input type="password" id="pw" name="password" class="form-control" placeholder="Password" required>
					</div>

					<div class="custom-control custom-checkbox mb-3">
						<input type="checkbox" class="custom-control-input show" id="customCheck1">
						<label class="custom-control-label" for="customCheck1">Lihat password</label>
					</div>
					<button class="btn btn-sm btn-primary btn-block text-uppercase" type="submit">Sign in</button>
					<hr class="my-4">
					<a href="daftar">Belum punya akun?</a>
				</form>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.sign.masuk', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/lelang_online/resources/views/sign/login.blade.php ENDPATH**/ ?>