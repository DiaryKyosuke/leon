<?php $__env->startSection('judul','Profile Anda'); ?>
<?php $__env->startSection('akuns','active'); ?>
<?php $__env->startSection('content'); ?>
<div class="row mt-3 justify-content-md-center">
	
	<div class="col-md-5">
		<div class="card shadow edit">
			<div class="card-body">
				<h5 class="card-title">Pengaturan Akun</h5>
				<form method="post" action="edit_user">
					<?php echo csrf_field(); ?>
					<div class="form-group">
						<input class="id" type="hidden" name="id" value="<?php echo e($data->id); ?>" required>

						<label>Nama Lengkap</label>
						<input type="text" class="form-control form-control-sm" name="nama" value="<?php echo e($data->nama); ?>" required>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label>Email Aktif</label>
							<input type="text" class="form-control form-control-sm" name="email" value="<?php echo e($data->email); ?>" required>
						</div>
						<div class="form-group col-md-6">
							<label>No. HP / W.A</label>
							<input type="text" class="form-control form-control-sm" name="telp" value="<?php echo e($data->telp); ?>" required>
						</div>
					</div>
					<hr>
					<div class="form-group row float-right">
						<div class="col">
							<button type="submit" class="btn btn-primary btn-sm shadow">Simpan</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="col-md-6">
		<div class="card shadow edit">
			<div class="card-body">
				<h5 class="card-title">Ganti Password</h5>
				<form method="post" action="edit_password">
					<?php echo csrf_field(); ?>
					<div class="form-group">
						<input type="hidden" name="id" value="<?php echo e($data->id); ?>" required>

						<label>Password Lama</label>
						<input type="password" id="lama" class="pw form-control form-control-sm" name="pw_lama" required>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label>Password Baru</label>
							<input type="password" id="baru" class="pw form-control form-control-sm" name="pw_baru" required>
						</div>
						<div class="form-group col-md-6">
							<label>Konfirmasi Password</label>
							<input type="password" id="Konfirmasi" class="pw form-control form-control-sm" name="konfirmasi_pw" required>
						</div>
					</div>
					<div class="form-group">
						<div class="custom-control custom-checkbox mb-3">
							<input type="checkbox" class="custom-control-input show" id="customCheck1">
							<label class="custom-control-label" for="customCheck1" style="top: 25rem;">Lihat password</label>
						</div>
					</div>
					<hr>
					<div class="form-group row float-right">
						<div class="col">
							<button id="submit" type="submit" class="btn btn-primary btn-sm shadow">Simpan Perubahan</button>
							<button type="reset" class="btn btn-danger btn-sm shadow">Reset</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.user.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/lelang_online/resources/views/user/profil.blade.php ENDPATH**/ ?>