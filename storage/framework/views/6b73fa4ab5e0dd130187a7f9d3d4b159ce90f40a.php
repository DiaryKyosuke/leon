<?php $__env->startSection('beranda','active'); ?>
<?php $__env->startSection('content'); ?>
<!-- container -->
<div class="contents">
<div id="body" class="container">
	<!-- deskripsi -->
	<div class="row deskripsi">

		<div class="row">
			<div class="col-lg  wow animated fadeInLeft">
				<img src="<?php echo e(url('/foto/ui/lelang.png')); ?>" class="img-fluid">
			</div>
			<div class="col-lg wow animated fadeInRight">
				<h3>Pelaksanaan <span>lelang</span> sangat <span>mudah</span></h3>
				<p>Lelang menjadi lebih praktis karena dapat menawar barang dari mana saja, tanpa perlu hadir langsung.</p>
				<hr>
			</div>
		</div>
		<div class="m-5"></div>

		<div class="row">
			<div class="col-lg wow animated fadeInUp a">
				<img src="<?php echo e(url('/foto/ui/terpercaya.png')); ?>" class="img-fluid">
				<hr>
			</div>
			<div class="col-lg wow animated fadeInDown">
				<h3>Lelang <span>terpercaya</span> dan <span>kompetitif</span></h3>
				<p>Semua barang yang dilelang sudah dicek keasliannya. Cara penawaran lelang yang khas, tidak ada prioritas dan pembatasan peserta lelang.</p>
				<hr>
			</div>
			<div class="col-lg wow animated fadeInUp b">
				<img src="<?php echo e(url('/foto/ui/terpercaya.png')); ?>" class="img-fluid">
				<hr>
			</div>
		</div>
		<div class="m-5"></div>

		<div class="row">
			<div class="col-lg wow animated fadeInUp">
				<img src="<?php echo e(url('/foto/ui/aman.png')); ?>" class="img-fluid">
			</div>
			<div class="col-lg wow animated fadeInDown">
				<h3>Proses lelang yang <span>aman</span> dan <span>terjadwal</span></h3>
				<p>Pelelangan dipimpin dan dilaksanakan oleh petugas. Jatuh tempo / waktu dari barang yang dilelang sudah ditentukan dan disetujui pelelang.</p>
				<hr>
			</div>
		</div>
		<div class="m-5"></div>
	</div>
	<!-- end deskripsi -->
</div>
</div>
<!-- end container -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.ui', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/lelang_online/resources/views/home.blade.php ENDPATH**/ ?>