<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<table style="width: 100%;">
	<tr>
		<td><img src="foto/logo_pdf.png" style="width: 400px;" alt="IMG"></td>
	</tr>
</table>
	<br><div style="border: 2px solid black;"></div><br>
	<div class="col-md-14">
		<table width="100%">
			<tr>
			<td><h2>Laporan Pelelangan Online</h2></td>
				
			</tr>
			<tr>
				<td>
					<div class="form-group">
						<?php
						$awals = Carbon\Carbon::parse($awal)->formatLocalized('%d %B %Y');
						$akhirs = Carbon\Carbon::parse($akhir)->formatLocalized('%d %B %Y');
						?>
						<strong>Periode : <?php echo e($awals); ?> s/d <?php echo e($akhirs); ?></strong>
					</div>
				</td>
			</tr>
		</table>
	</div>

	<table class="table table-striped table-bordered">
		<thead class="thead-dark">
			<tr style="text-align: center;">
				<th scope="col">No. </th>
				<th scope="col">Tgl Ditutup</th>
				<th scope="col">Barang</th>
				<th scope="col">Pemenang</th>
				<th scope="col">Email Pemenang</th>
				<th scope="col">Hrg. Awal</th>
				<th scope="col">Hrg. Akhir</th>
			</tr>
		</thead>
		<tbody>
			<?php $i = 1; ?>
			<?php $__currentLoopData = $history; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $h): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr style="text-align: center;">
				<th scope="row"><?php echo e($i++); ?></th>
				<td><?php echo e(Carbon\Carbon::parse($h->tgl_selesai_lelang)->formatLocalized('%d %B %Y')); ?></td>
				<td><?php echo e($h->nama_barang); ?></td>
				<?php if($h->id_user == null): ?>
				<td style="background-color: #ff3333; color: white;">
					<span class="" >-</span>
				</td>
				<td>-</td>
				<?php else: ?>
				<td style="background-color: #1778f2; color: white;">
					<?php $data = $users->where('id','=',$h->id_user)->first(); ?>
					<span class=""><?php echo e($data->nama); ?></span>
				</td>
				<td><?php echo e($data->email); ?></td>
				<?php endif; ?>
				<td>Rp. <?php echo e(number_format($h->harga_awal, 0, ".", ".")); ?></td>
				<td>Rp. <?php echo e(number_format($h->harga_akhir, 0, ".", ".")); ?></td>
			</tr>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</tbody>
	</table>
</body>
</html><?php /**PATH /var/www/html/lelang_online/resources/views/admin/view_pdf.blade.php ENDPATH**/ ?>