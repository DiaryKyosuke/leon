<?php $__env->startSection('judul','List Petugas'); ?>
<?php $__env->startSection('member','active'); ?>
<?php $__env->startSection('content'); ?>
<nav aria-label="breadcrumb" class="mt-3">
	<h6>Member</h6>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="#"><i class="fas fa-users"></i> Member</a></li>
		<li class="breadcrumb-item active"><a href="#"></i> petugas</a></li>
	</ol>
</nav>

<div class="card mt-3">
	<div class="card-header">
		<div class="row">
			<div class="col">
				List Petugas
			</div>
			<div class="col text-right">
				<button style="font-size: 15px;" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#tambah"><i class="fas fa-plus"></i> Tambah</button>
			</div>
		</div>
	</div>
	<div class="card-body card-font">
		<!-- content -->
		<table id="example" class="table table-striped table-bordered table-hover" style="width: 100%;">
			<thead>
				<tr>
					<!-- <th>No.</th> -->
					<th style="max-width:100px;">Level</th>
					<th style="max-width:150px;">Nama</th>
					<th>Email</th>
					<th>Username</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php $__currentLoopData = $admin; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $a): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<?php
				if ($a->id_level == 1) {
					$level = "Administrator";
					$w = "primary";
				} else {
					$level = "Petugas";
					$w = "success";
				}
				?>
				<tr class="item<?php echo e($a->id_petugas); ?>">
					<td>
						<span class="btn-sm btn-<?php echo e($w); ?> lev"><?php echo e($level); ?></span>
					</td>
					<td>
						<?php echo e($a->nama); ?>

					</td>
					<td><?php echo e($a->email); ?></td>
					<td><?php echo e($a->username); ?></td>
					<td>
						<button class="btn btn-outline-primary btn-sm btn-ling edit-modal" data-id_petugas="<?php echo e($a->id_petugas); ?>" data-id_level="<?php echo e($a->id_level); ?>" data-name="<?php echo e($a->nama); ?>" data-email="<?php echo e($a->email); ?>" data-username="<?php echo e($a->username); ?>">
							<i class="fas fa-user-edit"></i>
						</button>
						<button class="btn btn-outline-danger btn-sm btn-ling delete-modal" data-id="<?php echo e($a->id_petugas); ?>" data-name="<?php echo e($a->nama); ?>">
							<i class="fas fa-trash-alt"></i>
						</button>
					</td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</tbody>
		</table>
	</div>
</div>
<!-- Modal create data -->
<div class="modal fade" id="tambah" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title" id="exampleModalLabel">
					Tambah Petugas
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="sample_form">
				<?php echo csrf_field(); ?>
				<div class="modal-body">
					<div class="form-group">
						<label>Nama Lengkap</label>
						<input type="text" class="form-control form-control-sm" name="nama" required>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label>Email Aktif</label>
							<input type="text" class="form-control form-control-sm" name="email" required>
						</div>
						<div class="form-group col-md-6">
							<label>Username</label>
							<input type="text" class="form-control form-control-sm" name="username" required>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label>Password</label>
							<input id="pas" type="password" class="pw form-control form-control-sm" name="password" required autocomplete>
						</div>
						<div class="form-group col-md-6">
							<label>Konfirmasi Password</label>
							<input id="kon" type="password" class="pw form-control form-control-sm" name="konfirmasi" required autocomplete>
						</div>
					</div>

					<div class="custom-control custom-radio custom-control-inline">
						<input name="level" type="radio" id="Admin" class="custom-control-input" value="1">
						<label class="custom-control-label" for="Admin">Admin</label>
					</div>
					<div class="custom-control custom-radio custom-control-inline">
						<input name="level" type="radio" id="Petugas" class="custom-control-input" value="2">
						<label class="custom-control-label" for="Petugas">Petugas</label>
					</div>
					<input type="hidden" name="id_level" id="level">
					<br><br>
					<div class="form-group">
						<div class="custom-control custom-checkbox mb-3">
							<input type="checkbox" class="custom-control-input lihat" id="customCheck1">
							<label class="custom-control-label" for="customCheck1">Lihat password</label>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" id="m" class="btn btn-primary btn-sm">Tambah</button>
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- end modal create -->

<!-- Modal edit data -->
<div class="modal fade" id="edit" role="dialog">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title" id="exampleModalLabel">
					Edit Petugas
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<input class="id_petugas" type="hidden" name="id_petugas" required>

					<label for="inputAddress">Nama Lengkap</label>
					<input type="text" class="nama form-control form-control-sm" name="nama" required>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label>Email Aktif</label>
						<input type="text" class="email form-control form-control-sm" name="email" required>
					</div>
					<div class="form-group col-md-6">
						<label>Username</label>
						<input type="text" class="username form-control form-control-sm" name="username">
					</div>
				</div>

				<div class="custom-control custom-radio custom-control-inline">
					<input name="level" type="radio" id="admin" class="admin custom-control-input">
					<label class="custom-control-label" for="admin">Admin</label>
				</div>
				<div class="custom-control custom-radio custom-control-inline">
					<input name="level" type="radio" id="petugas" class="petugas custom-control-input">
					<label class="custom-control-label" for="petugas">Petugas</label>
				</div>
				<input type="hidden" name="id_level" class="id_lev" required>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-sm edit">Update</button>
				<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- end modal edit -->

<!-- modal delete -->
<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title" id="exampleModalLabel">
					Hapus Data
				</div>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form class="sample_form">
				<div class="modal-body">
					Anda Yakin Untuk Menghapus Data <span class="dname"></span> ?? <span style="display: none;" class="hidden did"></span>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-danger btn-sm delete" data-dismiss="modal"><i class="fas fa-trash-alt"></i> Delete</button>
					<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- end modal delete -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('crud'); ?>
<script type="text/javascript" src="<?php echo e(url('js/admin/crud_petugas.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.admin.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/lelang_online/resources/views/admin/petugas.blade.php ENDPATH**/ ?>