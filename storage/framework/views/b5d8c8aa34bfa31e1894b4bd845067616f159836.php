<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php echo $__env->yieldContent('judul','Lelang Online'); ?></title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <!-- fancybox -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
    <!-- datepicker & clockpicker-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://weareoutman.github.io/clockpicker/dist/bootstrap-clockpicker.min.css">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('css/admin/custom.css')); ?>">

    <link rel="shortcut icon" href="<?php echo e(url('foto/logo.ico')); ?>" />
</head>

<body>
    <div class="wrapper">
        <!-- Iki Sidebar  -->
        <nav id="sidebar">
            <div id="dismiss">
                <i class="fas fa-times"></i>
            </div>

            <div class="sidebar-header">
             <center>
                 <!-- <img src=""> -->
                 <span><?php echo e(Auth::guard('admin')->user()->nama); ?></span>
             </center>
         </div>

         <ul class="list-unstyled components">
            <p><img src="<?php echo e(url('foto/logo2.png')); ?>" style="width: 200px;"></p>
            <li class="<?php echo $__env->yieldContent('dashboard',''); ?>">
                <a href="dashboard"><i class="fas fa-tachometer-alt"></i> Dashboard</a>

            </li>
            <?php if(Auth::guard('admin')->user()->id_level == 1): ?>
            <li class="<?php echo $__env->yieldContent('member',''); ?>">
                <a href="#member" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-users"></i> Member</a>
                <ul class="collapse list-unstyled" id="member">
                    <li>
                        <a href="petugas"><i class="fas fa-id-card-alt"></i> Petugas</a>
                    </li>
                    <li>
                        <a href="user"><i class="fas fa-id-badge"></i> User</a>
                    </li>
                </ul>
            </li>
            <?php else: ?>
            <?php endif; ?>
            <li class="<?php echo $__env->yieldContent('barang',''); ?>">
                <a href="barang"><i class="fas fa-cube"></i> Barang</a>
            </li>
            <li class="<?php echo $__env->yieldContent('profile',''); ?>">
                <a href="profile"><i class="fas fa-cog"></i> Pengaturan Akun</a>
            </li>
            <li class="<?php echo $__env->yieldContent('laporan',''); ?>">
                <a href="laporan"><i class="fas fa-address-card"></i> Cetak Laporan</a>
            </li>
        </ul>

        <ul class="list-unstyled CTAs">
            <li>
                <a href="logout" class="keluar">Logout</a>
            </li>
        </ul>
    </nav>

    <div class="coba"></div>
    <!-- end sidebar -->
    <div id="content">

        <nav class="navbar navbar-expand-lg navbar-dark">
            <div class="container-fluid">

                <button type="button" id="sidebarCollapse" class="togglemenu btn">
                    <i class="fas fa-align-left"></i>
                    <span>Menu</span>
                </button>
                <a href="logout" class="btn d-inline-block d-lg-none ml-auto cus" type="button">
                    <i class="fas fa-sign-out-alt"></i>
                </a>

                <!-- navbar -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="text-white" href="logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <?php echo $__env->yieldContent('content'); ?>
        <footer>

        </footer>

    </div>
    <div class="overlay"></div>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <?php echo $__env->make('sweet::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="https://weareoutman.github.io/clockpicker/dist/bootstrap-clockpicker.min.js"></script>
    <script type="text/javascript" src="<?php echo e(url('js/admin/custom.js')); ?>"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/rowreorder/1.2.6/js/dataTables.rowReorder.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>
    <?php echo $__env->yieldContent('crud'); ?>
</body>

</html><?php /**PATH /var/www/html/lelang_online/resources/views/layouts/admin/home.blade.php ENDPATH**/ ?>