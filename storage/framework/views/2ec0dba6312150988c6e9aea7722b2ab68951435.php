 <!DOCTYPE html>
 <html>

 <head>
 	<meta charset="utf-8">
 	<meta name="viewport" content="width=device-width, initial-scale=1.0">
 	<meta http-equiv="X-UA-Compatible" content="IE=edge">

 	<title><?php echo $__env->yieldContent('judul','Page Dashboard'); ?></title>

 	<!-- Bootstrap CSS CDN -->
 	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
 	<!-- Font Awesome JS -->
 	<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
 	<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
 	<!-- fancybox -->
 	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css">
 	<!-- css gawean dewe -->
 	<link rel="stylesheet" type="text/css" href="<?php echo e(url('css/user/costom.css')); ?>">
 </head>
 <body>
 	<div class="wrapper">
 		<!-- Iki Sidebar responsive -->
 		<nav id="sidebar">
 			<div id="x">
 				<i class="fas fa-times"></i>
 			</div>
 			<div class="sidebar-header">
 				<center>
 					<img src="">
 					<span>Lelang Online</span>
 				</center>
 			</div>
 		</ul>
 		<ul class="list-unstyled CTAs">
 			<li>
 				<a href="sign" class="keluar">Login</a>
 			</li>
 			<li>
 				<a href="daftar" class="keluar">Daftar</a>
 			</li>
 		</ul>
 	</nav>
 	<!-- end sidebar -->
 	<div id="content">
 		<nav class="navbar navbar-expand-lg navbar-dark">
 			<div class="container-fluid">
 				<!-- navbar ketika responsive -->
 				<button type="button" id="sidebarCollapse" class="togglemenu btn">
 					<i class="fas fa-align-left"></i>
 				</button>

 				<!-- navbar dekstop -->
 				<div class="collapse navbar-collapse" id="navbarSupportedContent" style="padding: 16px;">
 					<ul class="nav navbar-nav ml-auto">
 						<li class="nav-item mr-3">
 							<a class="text-white" href="sign"><i class="fas fa-sign-in-alt"></i> Login</a>
 						</li>
 						<li class="nav-item">
 							<a class="text-white" href="daftar"><i class="fas fa-edit"></i> Daftar</a>
 						</li>
 					</ul>
 				</div>
 			</div>
 		</nav>

 		<?php echo $__env->yieldContent('content'); ?>

 		<!-- Site footer -->
 		<footer>
 		</footer>

 	</div>
 	<div class="overlay"></div>

 	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
 	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
 	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
 	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
 	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>
 	<script type="text/javascript" src="<?php echo e(url('js/user/custom.js')); ?>"></script>
 </body>
 </html>
<?php /**PATH /var/www/html/lelang_online/resources/views/layouts/home.blade.php ENDPATH**/ ?>