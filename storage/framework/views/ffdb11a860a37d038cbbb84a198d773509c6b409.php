<?php $__env->startSection('judul','History'); ?>
<?php $__env->startSection('pelelangan','aktif'); ?>
<?php $__env->startSection('history','on'); ?>
<?php $__env->startSection('lels','active'); ?>
<?php $__env->startSection('content'); ?>
<style type="text/css">
	.lev{
		border-radius: 16px; 
		font-size: 12px;
	}
	.card-font{
        font-size: 15px;
    }
</style>
<div class="card mt-3">
	<div class="card-header">
		<div class="row">
			<div class="col">
				History Lelang
			</div>
		</div>
	</div>
	<div class="card-body card-font">
		<!-- content -->
		<table id="example" class="table table-striped table-bordered table-hover" style="width: 100%;">
			<thead>
				<tr style="text-align: center;">
					<th style="width: 5px;">No.</th>
					<th style="width: 150px;">Nama Barang</th>
					<th style="width: 160px;">Pemenang</th>
					<th style="width: 80px;">Hrg. Awal</th>
					<th style="width: 80px;">Hrg. Akhir</th>
					<th style="width: 10px;">Status</th>
					<th style="width: 10px;">Rincian</th>
				</tr>
			</thead>
			<tbody>
				<?php $__currentLoopData = $barang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $b): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<tr>
					<td style="text-align: center;"><?php echo e(++$i); ?></td>
					<td><?php echo e($b->nama_barang); ?></td>
					<td style="text-align: center;"> 
						<?php if($b->id_user == null): ?>
						<button type="button" style="cursor: default;" class="btn btn-danger btn-sm lev">Tidak Ada Pemenang</button>
						<?php else: ?>
						<?php if ($b->id_user !== $users->id): ?>
							<?php $data = $nama->where('id','=',$b->id_user)->first(); ?>
							<button type="button" style="cursor: default;" class="btn btn-info btn-sm lev"><?php echo e($data->nama); ?></button>
						<?php else: ?>
							<button type="button" style="cursor: default;" class="btn btn-primary btn-sm lev">Anda Pemenang Lelang</button>
						<?php endif ?>
						<?php endif; ?>
					</td>
					<td style="text-align: center;">Rp. <?php echo e(number_format($b->harga_awal, 0, ".", ".")); ?></td>
					<td style="text-align: center;">Rp. <?php echo e(number_format($b->harga_akhir, 0, ".", ".")); ?></td>
					<td style="text-align: center;">
						<?php if($b->status == 'Ditutup'): ?>
						<button type="button" style="cursor: default;" class="btn btn-danger btn-sm lev">Lelang <?php echo e($b->status); ?></button>
						<?php else: ?>
						<button type="button" style="cursor: default;" class="btn btn-primary btn-sm lev">Lelang <?php echo e($b->status); ?></button>
						<?php endif; ?>
					</td>
					<?php $id_user = Auth::guard('user')->user()->id; ?>
					<td style="text-align: center;"><a href="rincian.<?php echo e($b->id_barang); ?>.<?php echo e($id_user); ?>" style="cursor: default;" class="btn btn-dark btn-sm lev">Detail</a></td>
				</tr>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</tbody>
		</table>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.user.home', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/lelang_online/resources/views/user/history.blade.php ENDPATH**/ ?>