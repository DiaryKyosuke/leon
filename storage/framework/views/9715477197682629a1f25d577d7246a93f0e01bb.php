
<?php $__currentLoopData = $barang; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $b): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<tr class="item<?php echo e($b->id_barang); ?>">
	<td><?php echo e($b->nama_barang); ?></td>
	<td>Rp. <?php echo e(number_format($b->harga_awal, 0, ".", ".")); ?></td>
	<td class="text-center">
		<?php if($b->tgl_mulai_lelang == null): ?>
		-
		<?php else: ?>
		$b->tgl_mulai_lelang
		<?php endif; ?>
	</td>
	<td><?php echo e($b->status); ?></td>
	<td>
		<button class="btn btn-outline-primary btn-sm btn-ling lelang-modal" data-id_barang="<?php echo e($b->id_barang); ?>" data-id_lelang="<?php echo e($b->id_lelang); ?>" data-nama="<?php echo e($b->nama_barang); ?>">
			<i class="fas fa-play"></i>
		</button>
		<button class="btn btn-outline-dark btn-sm btn-ling info-modal" data-id="<?php echo e($b->id_barang); ?>" data-nama="<?php echo e($b->nama_barang); ?>" data-harga_awal="<?php echo e(number_format($b->harga_awal, 0, '.', '.')); ?>" data-harga_akhir="<?php echo e(number_format($b->harga_akhir, 0, '.', '.')); ?>" data-deskripsi="<?php echo e($b->deskripsi_barang); ?>" data-gambar="<?php echo e($b->foto_barang); ?>" data-status="<?php echo e($b->status); ?>" data-tgl_mulai="<?php echo e($b->tgl_mulai_lelang); ?>" data-tgl_selesai="<?php echo e($b->tgl_selesai_lelang); ?>">
			<i class="fas fa-info"></i>
		</button>
		<button class="btn btn-outline-primary btn-sm btn-ling edit-modal" data-id="<?php echo e($b->id_barang); ?>" data-nama="<?php echo e($b->nama_barang); ?>" data-harga="<?php echo e(number_format($b->harga_awal, 0, '.', '.')); ?>" data-deskripsi="<?php echo e($b->deskripsi_barang); ?>" data-gambar="<?php echo e($b->foto_barang); ?>">
			<i class="fas fa-user-edit"></i>
		</button>
		<button class="btn btn-outline-danger btn-sm btn-ling delete-modal" data-id="<?php echo e($b->id_barang); ?>" data-name="<?php echo e($b->nama_barang); ?>">
			<i class="fas fa-trash-alt"></i>
		</button>
	</td>
</tr>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php /**PATH /var/www/html/lelang_online/resources/views/admin/test.blade.php ENDPATH**/ ?>