-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 17, 2020 at 10:30 AM
-- Server version: 5.7.28-0ubuntu0.19.04.2
-- PHP Version: 7.2.24-0ubuntu0.19.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lelang`
--
CREATE DATABASE IF NOT EXISTS `lelang` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `lelang`;

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id_petugas` int(10) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_level` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id_petugas`, `nama`, `email`, `username`, `password`, `id_level`, `created_at`, `updated_at`) VALUES
(144, 'Fendi Anwar Rifa\'i', 'admin@gmail.com', 'admin', '$2y$10$MmWqNzJV/vwZU4Uf6Qn6EeoO9e8TCJ3wurHcfBHwkDE.p.UVJngIS', 1, '2020-03-02 00:51:18', '2020-03-23 15:15:48'),
(147, 'petugas saya', 'petugas@gmail.com', 'petugas', '$2y$10$tCdSGZ8OPpxbmMqO8m68rex6IbTFiRXS7lbFdQOIgW9iovARm4Lp6', 2, '2020-03-02 07:37:06', '2020-03-29 02:51:45'),
(148, 'asw', 'asw', 'asw', '$2y$10$5jV3To5woFFHeG.BBh1yrOqgmEw1a5DhvJCH9pVGr8yenOzH5cv2G', 2, '2020-03-05 00:02:51', '2020-03-05 00:02:51'),
(149, 'coba', 'coba@gmail.com', 'coba', '$2y$10$upvH6wD4TrjYUt2n1jqRley9J5tt7PCde/Ua0xAlTNeOMmnN.cTlW', 1, '2020-03-14 08:24:28', '2020-03-23 12:21:25'),
(150, 'admin', 'admin', 'admin', '$2y$10$k.9/Ccyciwa4IdNzmJboy.hvvEwqWeueuhXq71LaTk1Wtvzi5M6ui', 1, '2020-03-23 15:11:25', '2020-03-23 15:11:25');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(10) UNSIGNED NOT NULL,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto_barang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_mulai_lelang` datetime DEFAULT NULL,
  `harga_awal` int(11) DEFAULT NULL,
  `deskripsi_barang` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `foto_barang`, `tgl_mulai_lelang`, `harga_awal`, `deskripsi_barang`, `created_at`, `updated_at`) VALUES
(128, 'hp Xiaomi Redmi Note 4', 'WhatsApp Image 2019-10-17 at 12.53.45.jpeg', '2020-03-23 22:42:00', 200000, 'nominus<br>\r\nsegel<br>\r\nkelengkapan fullset', '2020-03-23 15:39:07', '2020-03-23 15:40:15'),
(129, 'Prosesor', '160---Heat-Proof-Monster--4K.jpg', '2020-03-23 23:25:00', 2000000, 'kondisi baru<br>\r\ntidak cacat', '2020-03-23 16:16:16', '2020-03-23 16:22:17'),
(130, 'script', 'pull dulu baru merge.png', '2020-03-24 21:30:00', 150000, 'script<br>\r\nscript', '2020-03-23 17:38:02', '2020-03-24 08:30:06'),
(131, 'oke', 'kisspng-computer-icons-avatar-user-login-5ae149b20c8348.1680096815247139060513.jpg', '2020-03-27 19:54:00', 2000, 'edit', '2020-03-23 17:38:23', '2020-03-24 08:49:50'),
(132, 'testing', 'Screenshot 2020-03-11 13:49:32.png', '2020-03-26 18:30:00', 200000, 'testing<br>\r\ntesting', '2020-03-24 08:43:49', '2020-03-24 08:44:13'),
(134, 'edit', 'Planet Pulsa PLN.png', '2020-03-24 18:30:00', 20000, 'edit<br>\r\nedit<br>\r\nedit', '2020-03-24 08:59:37', '2020-03-24 09:03:06'),
(135, 'testing', 'lanjutan.png', '2020-03-27 22:13:00', 20000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br>\r\n		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br>\r\n		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br>\r\n		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br>\r\n		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>\r\n		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2020-03-27 15:05:56', '2020-03-27 15:06:26'),
(136, 'coba coba', 'Screenshot 2020-03-11 13:49:32.png', '2020-04-09 19:33:00', 120000, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\n		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\n		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2020-04-09 12:29:47', '2020-04-09 12:30:10'),
(137, 'mencoba', 'kisspng-computer-icons-avatar-user-login-5ae149b20c8348.1680096815247139060513.jpg', '2020-04-29 00:00:00', 121212, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br>\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br>\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br>\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br>\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2020-04-10 17:22:29', '2020-04-10 17:22:58'),
(138, 'testing sek', 'pull dulu baru merge.png', '2020-04-15 17:46:00', 3344, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br>\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br>\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br>\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br>\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2020-04-10 17:22:43', '2020-04-10 17:23:15'),
(139, 'test bro', 'Screenshot 2020-03-11 13:49:32.png', '2020-04-11 09:02:00', 234324, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br>\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br>\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br>\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br>\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2020-04-10 22:44:06', '2020-04-10 22:44:23'),
(140, 'laptop', 'Planet Pulsa PLN.png', '2020-04-29 18:30:00', 32223, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br>\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br>\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br>\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br>\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2020-04-10 22:45:53', '2020-04-10 22:46:10'),
(141, 'oke oke gan', 'kisspng-computer-icons-avatar-user-login-5ae149b20c8348.1680096815247139060513.jpg', '2020-04-29 07:35:00', 234342, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br>\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br>\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br>\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br>\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2020-04-11 00:01:03', '2020-04-11 00:02:04'),
(142, 'mouse', '13839819_B.jpg', '2020-04-30 18:30:00', 4353, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br>\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br>\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br>\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br>\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2020-04-11 00:01:13', '2020-04-11 00:02:15'),
(143, 'tv', 'Planet Pulsa PLN.png', '2020-04-29 18:30:00', 34234, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br>\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br>\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br>\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br>\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2020-04-11 00:01:26', '2020-04-11 00:02:25'),
(144, 'kipas angin', 'lanjutan.png', '2020-04-30 07:35:00', 34324, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br>\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br>\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br>\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br>\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2020-04-11 00:01:39', '2020-04-11 00:02:36'),
(145, 'tripod', '8867.Microsoft_5F00_Logo_2D00_for_2D00_screen.jpg', '2020-04-29 20:40:00', 324, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br>\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br>\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br>\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br>\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br>\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '2020-04-11 00:01:49', '2020-04-11 00:03:32'),
(146, 'hp', 'WhatsApp Image 2019-10-17 at 12.53.44.jpeg', '2020-04-15 17:46:00', 200000, 'nominus<br>\r\nnormal<br>\r\nfullset', '2020-04-15 05:20:16', '2020-04-15 05:21:24'),
(147, 'hp hp', 'WhatsApp Image 2019-10-17 at 12.53.44.jpeg', NULL, 20000, 'no minus<br>\r\nfullset', '2020-04-15 11:18:09', '2020-04-15 11:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `history_lelang`
--

CREATE TABLE `history_lelang` (
  `id_history` int(10) UNSIGNED NOT NULL,
  `id_lelang` int(10) UNSIGNED NOT NULL,
  `id_barang` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `penawaran_harga` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `history_lelang`
--

INSERT INTO `history_lelang` (`id_history`, `id_lelang`, `id_barang`, `id_user`, `penawaran_harga`, `created_at`, `updated_at`) VALUES
(22, 109, 128, 8, 205000, '2020-03-23 15:52:58', '2020-03-23 15:52:58'),
(23, 115, 134, 8, 21000, '2020-03-26 10:42:54', '2020-03-26 10:42:54'),
(24, 113, 132, 8, 205000, '2020-03-26 15:18:27', '2020-03-26 15:18:27'),
(25, 113, 132, 9, 250000, '2020-03-26 15:20:19', '2020-03-26 15:20:19'),
(26, 113, 132, 8, 280000, '2020-03-26 15:20:33', '2020-03-26 15:20:33'),
(27, 113, 132, 9, 350000, '2020-03-26 15:20:42', '2020-03-26 15:20:42'),
(28, 112, 131, 8, 2500, '2020-03-27 13:31:47', '2020-03-27 13:31:47'),
(29, 112, 131, 8, 26000, '2020-03-27 13:31:51', '2020-03-27 13:31:51'),
(30, 112, 131, 8, 27000, '2020-03-27 13:31:56', '2020-03-27 13:31:56'),
(31, 112, 131, 8, 28000, '2020-03-27 13:31:59', '2020-03-27 13:31:59'),
(32, 112, 131, 8, 29000, '2020-03-27 13:32:02', '2020-03-27 13:32:02'),
(33, 112, 131, 9, 30000, '2020-03-27 13:32:32', '2020-03-27 13:32:32'),
(34, 120, 139, 8, 250000, '2020-04-12 01:30:13', '2020-04-12 01:30:13'),
(35, 120, 139, 8, 255000, '2020-04-12 01:30:34', '2020-04-12 01:30:34'),
(36, 120, 139, 8, 256000, '2020-04-12 01:30:42', '2020-04-12 01:30:42'),
(37, 120, 139, 8, 256100, '2020-04-12 01:33:32', '2020-04-12 01:33:32'),
(38, 120, 139, 8, 256200, '2020-04-12 01:35:02', '2020-04-12 01:35:02'),
(39, 120, 139, 8, 256300, '2020-04-12 01:37:40', '2020-04-12 01:37:40'),
(40, 120, 139, 8, 256400, '2020-04-12 01:38:06', '2020-04-12 01:38:06'),
(41, 120, 139, 8, 256500, '2020-04-12 01:38:19', '2020-04-12 01:38:19'),
(42, 120, 139, 8, 256600, '2020-04-12 01:38:49', '2020-04-12 01:38:49'),
(43, 120, 139, 8, 256700, '2020-04-12 01:39:18', '2020-04-12 01:39:18'),
(44, 120, 139, 8, 256710, '2020-04-12 01:40:39', '2020-04-12 01:40:39'),
(45, 120, 139, 8, 257000, '2020-04-12 01:41:52', '2020-04-12 01:41:52'),
(46, 120, 139, 8, 257100, '2020-04-12 01:42:54', '2020-04-12 01:42:54'),
(47, 120, 139, 8, 257110, '2020-04-12 01:44:20', '2020-04-12 01:44:20'),
(48, 120, 139, 8, 257120, '2020-04-12 01:52:35', '2020-04-12 01:52:35'),
(49, 120, 139, 8, 257130, '2020-04-12 01:53:16', '2020-04-12 01:53:16'),
(50, 120, 139, 7, 257140, '2020-04-12 01:56:29', '2020-04-12 01:56:29'),
(51, 120, 139, 7, 257150, '2020-04-12 01:56:59', '2020-04-12 01:56:59'),
(52, 120, 139, 8, 257160, '2020-04-12 04:07:06', '2020-04-12 04:07:06'),
(53, 120, 139, 9, 257170, '2020-04-12 10:20:49', '2020-04-12 10:20:49'),
(54, 120, 139, 9, 257180, '2020-04-12 10:31:37', '2020-04-12 10:31:37'),
(55, 120, 139, 9, 257190, '2020-04-12 10:32:32', '2020-04-12 10:32:32'),
(56, 120, 139, 8, 257191, '2020-04-12 10:32:45', '2020-04-12 10:32:45'),
(57, 120, 139, 8, 257192, '2020-04-12 12:58:23', '2020-04-12 12:58:23'),
(58, 120, 139, 8, 257193, '2020-04-12 13:17:45', '2020-04-12 13:17:45'),
(59, 120, 139, 8, 257194, '2020-04-12 13:30:09', '2020-04-12 13:30:09'),
(60, 120, 139, 8, 257195, '2020-04-12 13:49:27', '2020-04-12 13:49:27'),
(61, 120, 139, 8, 257196, '2020-04-12 13:51:10', '2020-04-12 13:51:10'),
(62, 120, 139, 8, 257197, '2020-04-12 13:55:06', '2020-04-12 13:55:06'),
(63, 120, 139, 8, 257198, '2020-04-12 13:55:32', '2020-04-12 13:55:32'),
(64, 120, 139, 8, 257199, '2020-04-12 13:59:05', '2020-04-12 13:59:05'),
(65, 120, 139, 8, 257200, '2020-04-12 14:01:07', '2020-04-12 14:01:07'),
(66, 120, 139, 7, 257201, '2020-04-12 14:02:58', '2020-04-12 14:02:58'),
(67, 120, 139, 8, 257202, '2020-04-12 14:54:25', '2020-04-12 14:54:25'),
(68, 120, 139, 8, 257203, '2020-04-12 14:59:05', '2020-04-12 14:59:05'),
(69, 120, 139, 7, 257204, '2020-04-12 14:59:12', '2020-04-12 14:59:12'),
(70, 120, 139, 8, 258000, '2020-04-15 05:09:52', '2020-04-15 05:09:52'),
(71, 120, 139, 7, 259000, '2020-04-15 05:11:29', '2020-04-15 05:11:29'),
(72, 117, 136, 8, 125000, '2020-04-15 05:43:33', '2020-04-15 05:43:33'),
(73, 127, 146, 8, 210000, '2020-04-15 11:09:28', '2020-04-15 11:09:28'),
(74, 127, 146, 7, 220000, '2020-04-15 11:10:33', '2020-04-15 11:10:33');

-- --------------------------------------------------------

--
-- Table structure for table `ikut`
--

CREATE TABLE `ikut` (
  `id_ikut` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_barang` int(10) UNSIGNED NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ikut`
--

INSERT INTO `ikut` (`id_ikut`, `id_user`, `id_barang`, `status`, `created_at`, `updated_at`) VALUES
(13, 8, 128, 'Diikuti', '2020-03-23 15:52:45', '2020-03-23 15:52:45'),
(14, 8, 134, 'Diikuti', '2020-03-26 10:42:42', '2020-03-26 10:42:42'),
(15, 8, 132, 'Diikuti', '2020-03-26 14:22:25', '2020-03-26 14:22:25'),
(16, 9, 132, 'Diikuti', '2020-03-26 14:59:20', '2020-03-26 14:59:20'),
(17, 9, 130, 'Diikuti', '2020-03-26 15:01:48', '2020-03-26 15:01:48'),
(18, 8, 131, 'Diikuti', '2020-03-27 13:22:26', '2020-03-27 13:22:26'),
(19, 9, 131, 'Diikuti', '2020-03-27 13:32:23', '2020-03-27 13:32:23'),
(20, 8, 139, 'Diikuti', '2020-04-12 01:22:04', '2020-04-12 01:22:04'),
(21, 7, 139, 'Diikuti', '2020-04-12 01:56:12', '2020-04-12 01:56:12'),
(22, 9, 139, 'Diikuti', '2020-04-12 10:20:35', '2020-04-12 10:20:35'),
(23, 8, 136, 'Diikuti', '2020-04-15 05:43:02', '2020-04-15 05:43:02'),
(24, 7, 136, 'Diikuti', '2020-04-15 05:43:04', '2020-04-15 05:43:04'),
(25, 8, 146, 'Diikuti', '2020-04-15 11:08:55', '2020-04-15 11:08:55'),
(26, 7, 146, 'Diikuti', '2020-04-15 11:10:14', '2020-04-15 11:10:14');

-- --------------------------------------------------------

--
-- Table structure for table `lelang`
--

CREATE TABLE `lelang` (
  `id_lelang` int(10) UNSIGNED NOT NULL,
  `id_barang` int(10) UNSIGNED NOT NULL,
  `tgl_selesai_lelang` datetime DEFAULT NULL,
  `harga_akhir` int(11) DEFAULT NULL,
  `id_user` int(10) UNSIGNED DEFAULT NULL,
  `id_petugas` int(10) UNSIGNED DEFAULT NULL,
  `status` enum('Akan Datang','Dibuka','Ditutup','Belum Diatur') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lelang`
--

INSERT INTO `lelang` (`id_lelang`, `id_barang`, `tgl_selesai_lelang`, `harga_akhir`, `id_user`, `id_petugas`, `status`, `created_at`, `updated_at`) VALUES
(109, 128, '2020-03-24 22:40:00', 205000, 8, 144, 'Ditutup', '2020-03-23 15:39:08', '2020-03-23 17:25:33'),
(110, 129, '2020-03-24 23:00:00', NULL, NULL, 144, 'Ditutup', '2020-03-23 16:16:16', '2020-03-25 13:19:36'),
(111, 130, '2020-03-28 18:00:00', NULL, NULL, 144, 'Ditutup', '2020-03-23 17:38:02', '2020-03-26 15:02:54'),
(112, 131, '2020-03-31 07:40:00', 30000, 9, 144, 'Ditutup', '2020-03-23 17:38:23', '2020-04-09 12:27:04'),
(113, 132, '2020-03-28 00:58:00', 350000, 9, 144, 'Ditutup', '2020-03-24 08:43:49', '2020-03-26 15:21:02'),
(115, 134, '2020-03-26 18:35:00', 21000, 8, 144, 'Ditutup', '2020-03-24 08:59:37', '2020-03-26 11:35:00'),
(116, 135, '2020-03-28 18:30:00', NULL, NULL, 144, 'Ditutup', '2020-03-27 15:05:57', '2020-03-29 00:29:56'),
(117, 136, '2020-04-30 18:30:00', NULL, NULL, 144, 'Dibuka', '2020-04-09 12:29:47', '2020-04-09 12:33:00'),
(118, 137, '2020-04-30 18:30:00', NULL, NULL, 144, 'Akan Datang', '2020-04-10 17:22:29', '2020-04-10 17:22:58'),
(119, 138, '2020-04-30 00:00:00', NULL, NULL, 144, 'Dibuka', '2020-04-10 17:22:43', '2020-04-10 17:23:15'),
(120, 139, '2020-05-12 20:25:00', 259000, 7, 144, 'Ditutup', '2020-04-10 22:44:06', '2020-04-15 05:22:20'),
(121, 140, '2020-04-30 18:30:00', NULL, NULL, 144, 'Akan Datang', '2020-04-10 22:45:53', '2020-04-10 22:46:10'),
(122, 141, '2020-04-30 18:30:00', NULL, NULL, 144, 'Akan Datang', '2020-04-11 00:01:03', '2020-04-11 00:02:04'),
(123, 142, '2020-05-01 17:25:00', NULL, NULL, 144, 'Akan Datang', '2020-04-11 00:01:13', '2020-04-11 00:02:15'),
(124, 143, '2020-04-30 18:30:00', NULL, NULL, 144, 'Akan Datang', '2020-04-11 00:01:26', '2020-04-11 00:02:25'),
(125, 144, '2020-05-01 18:30:00', NULL, NULL, 144, 'Akan Datang', '2020-04-11 00:01:39', '2020-04-11 00:02:36'),
(126, 145, '2020-04-30 19:35:00', NULL, NULL, 144, 'Akan Datang', '2020-04-11 00:01:49', '2020-04-11 00:03:33'),
(127, 146, '2020-04-30 00:00:00', NULL, NULL, 144, 'Dibuka', '2020-04-15 05:20:16', '2020-04-15 10:23:00'),
(128, 147, NULL, NULL, NULL, NULL, 'Belum Diatur', '2020-04-15 11:18:10', '2020-04-15 11:18:10');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id_level` int(10) UNSIGNED NOT NULL,
  `level` enum('admin','petugas') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id_level`, `level`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-02-27 06:27:57', '2020-02-27 06:27:57'),
(2, 'petugas', '2020-02-27 06:28:29', '2020-02-27 06:28:29');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_02_27_120636_create_level_table', 2),
(4, '2020_02_27_120646_create_admins_table', 3),
(5, '2020_03_04_073845_create_barang_table', 4),
(6, '2020_03_04_085403_create_lelang_table', 5),
(7, '2020_03_11_185103_create_tawar_table', 6),
(8, '2020_03_12_214952_create_ikut_table', 7),
(9, '2020_03_14_182242_create_history_lelang_table', 8);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `telp`, `email`, `username`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(7, 'alone', '081225364329', 'alone', 'alone', '$2y$10$qsWcUOm2cLs1Oh0htSUFmuhuZPTVCN8WjjxFFBzdGWJ/HPpsyKpeO', NULL, '2020-03-05 06:58:05', '2020-03-05 06:58:05'),
(8, 'Anwar Rifa\'i', '081225364329', 'users@gmail.com', 'users', '$2y$10$5neLbX5GKseXtiv6OamIduLl64aGAaZRCT4GhzFZpQOABgV6BdjDW', NULL, '2020-03-12 14:37:07', '2020-03-26 13:23:39'),
(9, 'testing', '081225364329', 'testing', 'testing', '$2y$10$rw7yY8CS92Y.fEFrHPfkr.pcRejQ5MD83zvzm.4V7RZBh1.Ve8IP6', NULL, '2020-03-12 14:38:19', '2020-03-12 14:38:19'),
(10, 'ujicoba', '081225364329', 'ujicoba', 'ujicoba', '$2y$10$acW7ZaB2PEg8xmxOgE/icONXcWJVyH2p59f93WIKcVr.E0rsYp50G', NULL, '2020-03-12 15:27:36', '2020-03-12 15:27:36'),
(11, 'Diary Kyosuke', '085964249369', 'fendianwar36@gmail.com', 'pengguna', '$2y$10$QaH4PHHUhF93eGYAk1gpGOKqYTMCLRukTGjRAEEgdd.iwkE/.vNHW', NULL, '2020-03-26 13:25:42', '2020-03-26 13:25:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id_petugas`),
  ADD KEY `admins_id_level_foreign` (`id_level`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- Indexes for table `history_lelang`
--
ALTER TABLE `history_lelang`
  ADD PRIMARY KEY (`id_history`),
  ADD KEY `history_lelang_id_barang_foreign` (`id_barang`),
  ADD KEY `history_lelang_id_lelang_foreign` (`id_lelang`),
  ADD KEY `history_lelang_id_user_foreign` (`id_user`);

--
-- Indexes for table `ikut`
--
ALTER TABLE `ikut`
  ADD PRIMARY KEY (`id_ikut`),
  ADD KEY `ikut_id_barang_foreign` (`id_barang`),
  ADD KEY `ikut_id_user_foreign` (`id_user`);

--
-- Indexes for table `lelang`
--
ALTER TABLE `lelang`
  ADD PRIMARY KEY (`id_lelang`),
  ADD KEY `lelang_id_user_foreign` (`id_user`),
  ADD KEY `lelang_id_petugas_foreign` (`id_petugas`),
  ADD KEY `lelang_id_barang_foreign` (`id_barang`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id_level`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id_petugas` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=148;
--
-- AUTO_INCREMENT for table `history_lelang`
--
ALTER TABLE `history_lelang`
  MODIFY `id_history` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=75;
--
-- AUTO_INCREMENT for table `ikut`
--
ALTER TABLE `ikut`
  MODIFY `id_ikut` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `lelang`
--
ALTER TABLE `lelang`
  MODIFY `id_lelang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=129;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id_level` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admins`
--
ALTER TABLE `admins`
  ADD CONSTRAINT `admins_id_level_foreign` FOREIGN KEY (`id_level`) REFERENCES `level` (`id_level`);

--
-- Constraints for table `history_lelang`
--
ALTER TABLE `history_lelang`
  ADD CONSTRAINT `history_lelang_id_barang_foreign` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_lelang_id_lelang_foreign` FOREIGN KEY (`id_lelang`) REFERENCES `lelang` (`id_lelang`) ON DELETE CASCADE,
  ADD CONSTRAINT `history_lelang_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `ikut`
--
ALTER TABLE `ikut`
  ADD CONSTRAINT `ikut_id_barang_foreign` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE,
  ADD CONSTRAINT `ikut_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `lelang`
--
ALTER TABLE `lelang`
  ADD CONSTRAINT `lelang_id_barang_foreign` FOREIGN KEY (`id_barang`) REFERENCES `barang` (`id_barang`) ON DELETE CASCADE,
  ADD CONSTRAINT `lelang_id_petugas_foreign` FOREIGN KEY (`id_petugas`) REFERENCES `admins` (`id_petugas`),
  ADD CONSTRAINT `lelang_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
