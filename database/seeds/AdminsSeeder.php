<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;
class AdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$faker = Faker::create('id_ID');

    	for($i = 1; $i <= 3; $i++){

    	      // insert data ke table pegawai menggunakan Faker
    		DB::table('admins')->insert([
    			'nama' => $faker->name,
                'email' => $faker->email,
    			'username' => $faker->userName,
    			'password' => bcrypt('alone'),
    			'id_level' => $faker->numberBetween(1,2),
    			'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
    			'updated_at' => Carbon::now()->format('Y-m-d H:i:s')

    		]);

    	}
    }
}
