<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryLelangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_lelang', function (Blueprint $table) {
            $table->Increments('id_history');
            $table->integer('id_lelang')->unsigned();
            $table->integer('id_barang')->unsigned();
            $table->integer('id_user')->unsigned();
            $table->integer('penawaran_harga');
            $table->timestamps();
            $table->foreign('id_lelang')->references('id_lelang')->on('lelang');
            $table->foreign('id_barang')->references('id_barang')->on('barang');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_lelang');
    }
}
