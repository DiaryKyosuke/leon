<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLelangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lelang', function (Blueprint $table) {
            $table->Increments('id_lelang');
            $table->integer('id_barang')->unsigned();
            $table->dateTime('tgl_selesai_lelang');
            $table->integer('harga_akhir');
            $table->integer('id_user')->unsigned();
            $table->integer('id_petugas')->unsigned();
            $table->enum('status',['Akan Datang','Dibuka','Ditutup']);
            $table->timestamps();
            $table->foreign('id_barang')->references('id_barang')->on('barang');
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_petugas')->references('id_petugas')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lelang');
    }
}
